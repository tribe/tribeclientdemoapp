//
//  TRBDevice.h
//  Tribe
//
//  Created by Petros Sfikakis on 25/09/2016.
//  Copyright © 2016 Tribe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

typedef enum {
    eeDataTypeBattery,
    eeDataTypeIMUBasic,
    eeDataTypeGTP2,
    eeDataTypeGTP3,
    eeDataTypeStartStop,
    eeDataTypeWeightHeight
} EEDataType;

@protocol TRBDeviceDelegate
- (void)hasDiscoveredAllCharacteristics;
- (void)commandRejectedDeviceIsProcessing;
- (void)didReadValueForType:(EEDataType)type data:(NSData*)data characteristic:(CBCharacteristic*)characteristic;

- (void)hasUpdatedBodyMeasurements;
@end

@interface TRBDevice : NSObject <CBPeripheralDelegate>

@property (nonatomic, weak) id<TRBDeviceDelegate> delegate;

@property (nonatomic, strong) NSString *devicePrefix;
@property (nonatomic, strong) NSString *deviceName;
@property (nonatomic, strong) NSString *deviceUUID;
@property (nonatomic) BOOL isConnected;
@property (nonatomic, strong) CBPeripheral *peripheral;

- (instancetype)initWithSlot:(NSString*)slot;

- (void)readStartStopState;

- (void)startCalibration;
- (void)stopCalibration;
- (void)resetToFactorySettings;

- (void)startRun;
- (void)setWeight:(uint16_t)weight height:(uint16_t)height;
- (void)stopRun;

- (void)readAllRunsList;
- (void)readDataForRunWithSlot:(uint8_t)slot;
- (void)deleteDataForRunWithSlot:(uint8_t)slot;
- (void)deleteAllRunData;

@end
