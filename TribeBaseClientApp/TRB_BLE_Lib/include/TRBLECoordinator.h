//
//  TRBLECoordinator.h
//  Tribe
//
//  Created by Petros Sfikakis on 05/08/16.
//  Copyright © 2016 Tribe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <UIKit/UIKit.h>
#import "TRBDevice.h"

@protocol BLECoordinatorDelegate
- (void)hasNotBeenConfigured;
- (void)bleIsAuthorized;
- (void)bleIsUnauthorizedWithState:(CBManagerState)bleAuthorizationState;
- (void)didDiscoverPeripheral:(CBPeripheral*)peripheral;
- (void)didConnectPeripheral:(CBPeripheral*)peripheral;
- (void)didDisconnectPeripheral:(CBPeripheral*)peripheral;
@end

@class TRPairDeviceViewController;

@interface TRBLECoordinator : NSObject <CBCentralManagerDelegate>

@property (nonatomic, weak) id<BLECoordinatorDelegate> delegate;

+ (instancetype)sharedCoordinator;
+ (NSString*)devicePrefix;
- (void)configureBLEManager;
- (TRBDevice*)connectedDevice;
- (void)startScan;
- (void)stopScan;
- (void)connectToDevice;
- (void)disconnectConnectedDevice;
- (void)setDeviceConnected:(BOOL)connected;

@end
