//
//  TRBCGTPWrapper.h
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 01/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Example_GetDeviceData.h"
#import "Example_GetEventData.h"
#import "Example_GetRunData.h"
#import "Example_RunInfoData.h"

@interface TRBCGTPWrapper : NSObject

+ (NSArray*)exampleGetDeviceData;
+ (NSDictionary*)exampleGetEventData;
+ (NSDictionary*)exampleGetRunData;
+ (NSDictionary*)exampleGetRunInfoData;

+ (NSDictionary*)convertToDictionary:(char*)input;
+ (FILE*)dataschemaFilePointer;

@end
