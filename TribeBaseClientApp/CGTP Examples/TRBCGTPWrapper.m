//
//  TRBCGTPWrapper.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 01/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBCGTPWrapper.h"

@implementation TRBCGTPWrapper

+ (NSArray*)exampleGetDeviceData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataSchema" ofType:@"json"];
    FILE *fp = fopen([filePath cStringUsingEncoding: NSUTF8StringEncoding], "r");
    char** output = Example_GetDeviceData(fp);
    NSMutableArray *array = [NSMutableArray new];
    for (int i=0; i<3; i++) {
        [array addObject:[TRBCGTPWrapper convertToDictionary:output[i]]];
    }
    for (int i=0; i < 3; i++){
        free(output[i]); // delete the json structure
        output[i] = 0;
    }
    free(output); // delete the array
    output = 0;
    return array;
}

+ (NSDictionary*)exampleGetEventData {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataSchema" ofType:@"json"];
    FILE *fp = fopen([filePath cStringUsingEncoding: NSUTF8StringEncoding], "r");
    char *output = Example_GetEventData(fp);
    return [TRBCGTPWrapper convertToDictionary:output];
}

+ (NSDictionary*)exampleGetRunData {
    char *output = Example_GetRunData();
    return [TRBCGTPWrapper convertToDictionary:output];
}

+ (NSDictionary*)exampleGetRunInfoData {
    char *output = Example_GetRunInfoData();
    return [TRBCGTPWrapper convertToDictionary:output];
}

+ (NSDictionary*)convertToDictionary:(char*)input {
    NSString *stringOutput = [NSString stringWithUTF8String:input];
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:[stringOutput dataUsingEncoding:NSUTF8StringEncoding]
                                                         options:kNilOptions
                                                           error:&error];
    return json;
}

+ (FILE*)dataschemaFilePointer {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataSchema" ofType:@"json"];
    FILE *fp = fopen([filePath cStringUsingEncoding: NSUTF8StringEncoding], "r");
    return fp;
}

@end
