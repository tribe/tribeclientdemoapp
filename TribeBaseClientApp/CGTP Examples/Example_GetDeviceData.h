/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef EXAMPLE_GETDEVICEDATA_H_INCLUDED
#define EXAMPLE_GETDEVICEDATA_H_INCLUDED

#include "gtp.h"
#include <stdint.h>

void GDD_GetDataSchema(FILE *fp);
uint8_t* GDD_SimulateBlePackets(int packetId);
int GDD_AddPacketToBufferAndCheck(uint8_t* packet);
void GDD_DecodeBlePackets();
char* GDD_DecodePacket(uint8_t *packet);
char** GDD_DecodeDeviceData(uint8_t *packet, FILE *fp);
char** GDD_DecodeDeviceDataInOne(FILE *fp, uint8_t* BinData, int BinDataLength);
char** Example_GetDeviceData(FILE *fp);

#endif // EXAMPLE_GETDEVICEDATA_H_INCLUDED
