/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef EXAMPLE_GETEVENTDATA_H_INCLUDED
#define EXAMPLE_GETEVENTDATA_H_INCLUDED

#include "gtp.h"

char *Example_GetEventData(FILE *fp);

void InitializeWithDataSchema(FILE *fp);
char *DecodeWithPacket(uint8_t *packet);


#endif // EXAMPLE_GETEVENTDATA_H_INCLUDED
