/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef EXAMPLE_RUNINFODATA_H_INCLUDED
#define EXAMPLE_RUNINFODATA_H_INCLUDED

#include "gtp.h"

char *Example_GetRunInfoData();
int AddPacketToBufferAndCheck(uint8_t* packet);
void DecodeBlePackets();

#endif // EXAMPLE_RUNINFODATA_H_INCLUDED
