/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */
#include "Example_GetDeviceData.h"

#define GDD_BUF 8192
char GDD_DataSchema[GDD_BUF];

void GDD_GetDataSchema(FILE *fp){
    int i;
    size_t nread;

    // Read the data schema from the json file DataSchema.json
    for (i=0; i<GDD_BUF; i++){
        GDD_DataSchema[i] = '\0';
    }

    if (fp == NULL){
        printf("Could not open file.");
    }else{
        while ((nread = fread(GDD_DataSchema, GDD_BUF, sizeof(char), fp)) > 0){}
    }
    fclose(fp);
}

uint8_t GDD_Device_Packets[10][182] = {
    {32, 0, 0, 38, 0, 0, 0, 3, 0, 0, 0, 11, 191, 176, 80, 160, 0, 182, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {32, 1, 0, 3, 1, 3, 177, 0, 0, 0, 218, 67, 0, 0, 160, 65, 0, 112, 218, 69, 0, 0, 128, 62, 0, 0, 128, 64, 128, 116, 23, 63, 132, 150, 19, 63, 0, 0, 112, 67, 11, 227, 229, 64, 37, 124, 56, 65, 37, 124, 56, 65, 170, 241, 210, 61, 134, 164, 169, 61, 64, 79, 8, 65, 221, 13, 108, 64, 244, 223, 3, 192, 236, 98, 101, 62, 232, 177, 167, 58, 195, 174, 123, 57, 106, 229, 159, 63, 136, 86, 26, 59, 167, 185, 162, 58, 159, 60, 104, 64, 68, 188, 101, 64, 177, 187, 9, 192, 148, 140, 24, 64, 46, 172, 140, 63, 196, 106, 144, 63, 14, 168, 140, 63, 131, 13, 6, 65, 228, 86, 133, 62, 76, 178, 116, 64, 177, 162, 26, 63, 8, 138, 27, 63, 39, 103, 101, 67, 247, 201, 227, 64, 134, 181, 83, 62, 89, 244, 23, 60, 249, 201, 32, 59, 8, 106, 115, 61, 47, 68, 106, 61, 93, 148, 145, 61, 178, 68, 170, 61, 44, 172},
    {32, 2, 0, 171, 61, 225, 3, 2, 3, 177, 0, 19, 174, 61, 39, 120, 204, 60, 28, 36, 145, 60, 57, 176, 13, 61, 26, 70, 41, 61, 117, 249, 39, 61, 9, 116, 21, 61, 244, 45, 13, 61, 33, 178, 33, 61, 129, 120, 21, 61, 74, 67, 43, 61, 227, 94, 47, 61, 185, 179, 70, 61, 77, 0, 40, 66, 130, 210, 247, 65, 124, 167, 66, 66, 243, 212, 70, 66, 17, 177, 67, 66, 111, 181, 43, 66, 178, 255, 103, 66, 96, 11, 138, 66, 133, 88, 77, 66, 13, 43, 73, 66, 238, 78, 76, 66, 145, 74, 100, 66, 42, 231, 233, 61, 120, 70, 126, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 75, 246, 79, 61, 54, 70, 52, 61, 224, 44, 113, 61, 116, 218, 128, 61, 75, 14, 132, 61, 10, 141, 133, 61, 180, 82, 172, 60, 156, 63, 103, 60, 62, 171, 235, 60, 110, 240, 254, 60},
    {32, 3, 0, 197, 139, 249, 60, 116, 138, 3, 3, 3, 82, 0, 246, 60, 225, 153, 243, 60, 159, 236, 244, 60, 129, 174, 246, 60, 176, 60, 2, 61, 180, 86, 11, 61, 217, 212, 15, 61, 47, 8, 37, 66, 227, 189, 0, 66, 11, 52, 67, 66, 124, 97, 69, 66, 101, 156, 60, 66, 113, 140, 57, 66, 209, 247, 106, 66, 14, 161, 135, 66, 244, 203, 76, 66, 132, 158, 74, 66, 155, 99, 83, 66, 143, 115, 86, 66, 117, 98, 243, 61, 53, 166, 102, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {32, 4, 0, 3, 1, 3, 177, 0, 0, 0, 218, 67, 0, 0, 32, 66, 0, 128, 79, 70, 0, 0, 128, 62, 0, 0, 128, 64, 175, 71, 16, 63, 123, 205, 13, 63, 0, 0, 112, 67, 243, 15, 238, 64, 218, 64, 177, 65, 218, 64, 177, 65, 164, 41, 85, 62, 18, 202, 40, 62, 29, 218, 1, 65, 101, 68, 12, 64, 113, 198, 151, 191, 207, 42, 80, 62, 92, 198, 96, 58, 193, 131, 13, 57, 5, 28, 7, 64, 199, 150, 181, 59, 82, 186, 0, 59, 219, 228, 118, 64, 189, 39, 111, 64, 177, 23, 214, 191, 239, 3, 4, 64, 101, 200, 131, 63, 9, 140, 109, 63, 153, 199, 131, 63, 138, 79, 251, 64, 12, 248, 131, 62, 206, 147, 120, 64, 210, 150, 15, 63, 208, 76, 16, 63, 146, 10, 105, 67, 13, 194, 234, 64, 21, 90, 62, 62, 218, 6, 83, 59, 12, 192, 82, 58, 159, 60, 222, 61, 233, 51, 163, 61, 96, 97, 233, 61, 225, 50, 247, 61, 108, 185},
    {32, 5, 0, 253, 61, 156, 3, 2, 3, 177, 0, 255, 253, 61, 255, 75, 68, 61, 117, 124, 199, 60, 30, 15, 104, 61, 160, 97, 96, 61, 182, 131, 89, 61, 75, 224, 106, 61, 63, 45, 120, 61, 151, 169, 98, 61, 162, 179, 106, 61, 17, 2, 135, 61, 145, 247, 144, 61, 119, 143, 136, 61, 212, 167, 48, 66, 212, 118, 244, 65, 31, 222, 70, 66, 11, 138, 53, 66, 21, 117, 43, 66, 102, 241, 56, 66, 43, 88, 95, 66, 75, 226, 138, 66, 225, 33, 73, 66, 246, 117, 90, 66, 235, 138, 100, 66, 155, 14, 87, 66, 140, 2, 37, 62, 160, 193, 126, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 236, 47, 193, 61, 243, 152, 163, 61, 176, 155, 222, 61, 30, 169, 233, 61, 70, 32, 241, 61, 65, 127, 246, 61, 115, 99, 26, 61, 161, 47, 177, 60, 112, 158, 72, 61, 197, 20, 89, 61},
    {32, 6, 0, 46, 163, 91, 61, 83, 152, 3, 3, 3, 82, 0, 98, 61, 98, 252, 103, 61, 23, 154, 110, 61, 243, 152, 116, 61, 119, 61, 122, 61, 178, 78, 131, 61, 23, 51, 133, 61, 226, 209, 27, 66, 190, 187, 222, 65, 55, 75, 53, 66, 99, 69, 58, 66, 13, 125, 54, 66, 7, 121, 56, 66, 30, 46, 116, 66, 16, 81, 144, 66, 200, 180, 90, 66, 157, 186, 85, 66, 243, 130, 89, 66, 250, 134, 87, 66, 19, 105, 54, 62, 88, 46, 118, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    {32, 7, 0, 3, 1, 3, 177, 0, 0, 0, 218, 67, 0, 0, 112, 66, 0, 92, 143, 70, 143, 194, 117, 62, 86, 85, 133, 64, 9, 67, 12, 63, 166, 169, 12, 63, 1, 0, 122, 67, 133, 3, 242, 64, 12, 223, 3, 66, 12, 223, 3, 66, 103, 216, 149, 62, 58, 81, 127, 62, 217, 126, 3, 65, 147, 226, 204, 63, 227, 137, 107, 191, 79, 120, 85, 62, 43, 196, 27, 58, 68, 79, 215, 56, 126, 27, 63, 64, 69, 159, 9, 60, 181, 251, 32, 59, 234, 182, 126, 64, 198, 186, 115, 64, 54, 156, 188, 191, 67, 111, 238, 63, 17, 210, 118, 63, 201, 49, 78, 63, 134, 197, 118, 63, 62, 240, 248, 64, 0, 200, 128, 62, 37, 241, 126, 64, 34, 213, 10, 63, 202, 118, 13, 63, 20, 2, 111, 67, 198, 146, 240, 64, 76, 37, 63, 62, 163, 54, 117, 58, 84, 46, 77, 57, 205, 218, 184, 61, 97, 138, 190, 61, 72, 162, 25, 62, 93, 100, 12, 62, 246, 212},
    {32, 8, 0, 13, 62, 71, 3, 2, 3, 177, 0, 183, 12, 62, 59, 243, 241, 60, 101, 187, 232, 60, 46, 12, 136, 61, 187, 54, 102, 61, 235, 130, 116, 61, 73, 78, 115, 61, 252, 187, 120, 61, 136, 91, 132, 61, 98, 56, 171, 61, 93, 173, 165, 61, 118, 104, 161, 61, 106, 199, 159, 61, 252, 226, 2, 66, 61, 73, 244, 65, 47, 27, 49, 66, 175, 250, 35, 66, 46, 101, 44, 66, 217, 231, 44, 66, 130, 142, 134, 66, 177, 237, 138, 66, 210, 228, 94, 66, 83, 5, 108, 66, 209, 154, 99, 66, 40, 24, 99, 66, 4, 36, 48, 62, 168, 37, 127, 63, 0, 0, 118, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 224, 149, 237, 61, 244, 225, 208, 61, 168, 228, 19, 62, 88, 1, 24, 62, 74, 251, 29, 62, 246, 186, 31, 62, 125, 230, 53, 61, 6, 250, 218, 60, 232, 61, 135, 61, 90, 108, 138, 61},
    {32, 9, 0, 102, 43, 144, 61, 153, 75, 3, 3, 3, 82, 0, 145, 61, 162, 162, 146, 61, 115, 35, 154, 61, 103, 139, 160, 61, 84, 150, 165, 61, 46, 203, 171, 61, 81, 42, 174, 61, 26, 246, 19, 66, 59, 69, 215, 65, 169, 88, 55, 66, 99, 101, 54, 66, 219, 226, 54, 66, 182, 20, 54, 66, 231, 9, 124, 66, 178, 46, 146, 66, 86, 167, 88, 66, 156, 154, 89, 66, 37, 29, 89, 66, 74, 235, 89, 66, 174, 168, 77, 62, 123, 222, 125, 63, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
};

uint8_t* GDD_SimulateBlePackets(int packetId){
    if (packetId >=0 && packetId <= 9)
        return &GDD_Device_Packets[packetId][0];
    return 0;
}

// return 1 when all the ble packets have been received and the buffer has been created successfully.
int GDD_AddPacketToBufferAndCheck(uint8_t* packet){
    // call the following function to pass the ble packet bytes to the buffer
    if ( AddPacketDataToTransBuffer(packet) < 0 )
    {
        printf("Error at packet %d.\n", 1);
        return -1;
    }

    // call the following function to check if ALL the ble packets received successfully.
    if ( CheckReceivedPackets() < 0 ){
         printf("Some packets may have been lost or did not received yet.\n");
         return -1;
    }
    else{
        // call the following function to check the Buffer.
        if ( CheckTransBuffer() < 0){
            printf("Problem with the Buffer.\n");
            return -1;
        }else{
            printf("The Buffer has been created successfully.\n");
            return 1;
        }
    }

    return 0;
}

void GDD_DecodeBlePackets(){
    Init_Transaction_3_Structures();

    Decoder( TR3_DataContainer,
             (uint32_t)TR3_ContainerLength,
             Transaction_3_SchemaDescription );

    Wrap_Transaction_3_Structures();
    Clear_Transaction_3_Structures();
}

char* GDD_DecodePacket(uint8_t *packet) {

    if (packet){
        // add the packet to the buffer and check if all
        // the packets have been received successfully.
        if ( GDD_AddPacketToBufferAndCheck(packet) == 1) {
            // _____ DECODE THE BUFFER _____
            GDD_DecodeBlePackets();
            // _____ BUILD THE JSON STRING _____
            char* jsonstr = GetTrans3Data(GDD_DataSchema);
            // _____ CLEAR THE DATA RELATED MEMEORY _____
            Clear_Transaction_3_Data();

            return jsonstr;
        }
    }

    return NULL;
}

char** GDD_DecodeDeviceData(uint8_t *packet, FILE *fp){
    if (packet){
        int i, lastPacketReceived;
        // add each packet to the buffer. When the last packet is received the function
        // TR42_AddPacket2Buffer( ) returns 1, else 0.
        lastPacketReceived = TR42_AddPacket2Buffer(packet);

        if ( lastPacketReceived == 1 ){
            // when the last packets is received you can get a json string with an array of lost packets (if any).
            char* jsonstr_missed = TR42_GetMissedPackets();
            printf("\n\nmissed packets: %s", jsonstr_missed);

            // Get the data byte-stream that can be decoded using the gtp
            uint8_t* BinData = 0;
            int BinDataLength = 0;
            TR42_GetTransBuffer(&BinData, &BinDataLength);

            if (BinData){
                // load the names of metrics that should be exported
                GDD_GetDataSchema(fp);

                char** jsonstr_array = (char**)malloc( TR42_PacketHeader_RunDataLength * sizeof(char*));

                printf("TR42_PacketHeader_RunDataLength: %d\n", TR42_PacketHeader_RunDataLength);

                BLE_Header bleHeader;
                int cnt = 0;
                for (i = 0; i < TR42_PacketHeader_RunDataLength * 3; i++){
                    char* jsonstr = GDD_DecodePacket(BinData + i * (bleHeader.Packet_Length_in_Bytes + BLE_PACKET_HEADER_SIZE) );

                    // read the ble packet header
                    ReadBleHeaderFromBytesArray(BinData, BLE_PACKET_SIZE, &bleHeader);

                    if ((jsonstr != NULL) && ( (i + 1) % 3 == 0)){
                        jsonstr_array[cnt] = jsonstr;
                        cnt++;
                    }
                }

//                if (jsonstr_array != NULL){
//                    for (i=0; i < TR42_PacketHeader_RunDataLength; i++){
//                        printf("jsonstr_array[%d]: %s\n", i, jsonstr_array[i]);
//                    }
//                }

                return jsonstr_array;
            }
        }
    }

    return NULL;
}

char** GDD_DecodeDeviceDataInOne(FILE *fp, uint8_t* BinData, int BinDataLength) {
    int i;
    
    if (BinData){
        // load the names of metrics that should be exported
        GDD_GetDataSchema(fp);
        
        char** jsonstr_array = (char**)malloc( TR42_PacketHeader_RunDataLength * sizeof(char*));
        
        printf("TR42_PacketHeader_RunDataLength: %d\n", TR42_PacketHeader_RunDataLength);
        
        BLE_Header bleHeader;
        int cnt = 0;
        for (i = 0; i < TR42_PacketHeader_RunDataLength * 3; i++){
            char* jsonstr = GDD_DecodePacket(BinData + i * (bleHeader.Packet_Length_in_Bytes + BLE_PACKET_HEADER_SIZE) );
            
            // read the ble packet header
            ReadBleHeaderFromBytesArray(BinData, BLE_PACKET_SIZE, &bleHeader);
            
            if ((jsonstr != NULL) && ( (i + 1) % 3 == 0)){
                jsonstr_array[cnt] = jsonstr;
                cnt++;
            }
        }
        return jsonstr_array;
    }
    
    return NULL;
}

char** Example_GetDeviceData(FILE *fp){
    int i;
    int RunDataLength = 3;

    char** jsonstr_array = NULL;

    for (i = 0; i < RunDataLength * 3 + 1; i++){
        // suppose that you receive the packets through ble
        uint8_t* packet = GDD_SimulateBlePackets(i);

        jsonstr_array = GDD_DecodeDeviceData(packet, fp); // it is used malloc internally. The jsonstr_array MUST be deallocated
    }

    if (jsonstr_array != NULL){
        for (i=0; i < RunDataLength; i++){
            printf("jsonstr_array[%d]: %s\n", i, jsonstr_array[i]);
        }
    }
    return jsonstr_array;
    // Here you must free the memory that has been allocated for the jsonstr_array !!!
//    for (i=0; i < RunDataLength; i++){
//        free(jsonstr_array[i]); // delete the json structure
//        jsonstr_array[i] = 0;
//    }
//    free(jsonstr_array); // delete the array
//    jsonstr_array = 0;
}
