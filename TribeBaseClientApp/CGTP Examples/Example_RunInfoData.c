/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

 #include "Example_RunInfoData.h"

 ////////////////////////////////////////////////////////////

// simulated ble packets

uint8_t IncomingPackets0[1][124] =
{
//  {31,1,1,93,0,0,0,186,66,103,1,1,0,102,2,3,0,26,0,12,0,30,0,0,0,5,0,0,0,10,86,206,62,171,223,85,66,148,247,162,64,224,188,254,64,2,1,38,0,0,0,3,0,0,0,58,81,127,62,12,223,3,66,92,205,63,64,176,222,253,64,2,2,39,0,0,0,1,0,0,0,87,85,213,61,245,200,39,65,82,32,70,63,205,73,193,64,2}
    {31, 1, 1, 119, 0, 0, 0, 238, 66, 103, 1, 1, 0, 102, 2, 4, 0, 26, 0, 12, 0, 30, 0, 0, 0, 5, 0, 0, 0, 10, 86, 206, 62, 171, 223, 85, 66, 148, 247, 162, 64, 224, 188, 254, 64, 2, 1, 38, 0, 0, 0, 3, 0, 0, 0, 58, 81, 127, 62, 12, 223, 3, 66, 92, 205, 63, 64, 176, 222, 253, 64, 2, 2, 39, 0, 0, 0, 1, 0, 0, 0, 87, 85, 213, 61, 245, 200, 39, 65, 82, 32, 70, 63, 205, 73, 193, 64, 2, 3, 41, 0, 0, 0, 10, 0, 0, 0, 14, 2, 131, 63, 202, 136, 8, 67, 162, 67, 27, 65, 62, 16, 0, 65, 2}
};


//uint8_t IncomingPackets[5][182] =
//{
//  {31,  1,  5,  177,  0,  0,  192,  70,  68,  103,  1,  1,  0,  102,  2,  30,  0,  26,  0,  12,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5},
//  {31,  2,  5,  177,  0,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0},
//  {31,  3,  5,  177,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21},
//  {31,  4,  5,  177,  0,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16},
//  {31,  5,  5,  87,  0,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
//};

uint8_t* SimulateBlePackets(int packetId){
    if (packetId >=0 && packetId <= 0)
        return &IncomingPackets0[packetId][0];
    return 0;
}

////////////////////////////////////////////////////////////

// Some useful functions

// return 1 when all the ble packets have been received and the buffer has been created successfully.
int AddPacketToBufferAndCheck(uint8_t* packet) {
    // call the following function to pass the ble packet bytes to the buffer
    if ( AddPacketDataToTransBuffer(packet) < 0 )
    {
        printf("Error at packet %d.\n", 1);
        return -1;
    }

    // call the following function to check if ALL the ble packets received successfully.
    if ( CheckReceivedPackets() < 0 ){
         printf("Some packets may have been lost or did not received yet.\n");
         return -1;
    }
    else{
        // call the following function to check the Buffer.
        if ( CheckTransBuffer() < 0){
            printf("Problem with the Buffer.\n");
            return -1;
        }else{
            printf("The Buffer has been created successfully.\n");
            return 1;
        }
    }

    return 0;
}


void DecodeBlePackets() {
    Init_transaction_41_Structures();

    Decoder( TR41_DataContainer,
             (uint32_t)TR41_ContainerLength,
             transaction_41_SchemaDescription );

    Wrap_transaction_41_Structures();
    Clear_transaction_41_Structures();
}


char *Example_GetRunInfoData()
{
    int i;

    /*
     *  _____  GET THE BLE PACKETS AND ADD THEIR CONTENTS TO THE BUFFER  __________________________________________________
     *
     */

    int error_flag = 0;
    for (i=0; i<1000; i++){
        // suppose that you just received the first ble packet from the WISH.
        uint8_t* packet = SimulateBlePackets(i);

        if (packet){
            // add the packet to the buffer and check if all the packets have been received successfully.
            if ( AddPacketToBufferAndCheck(packet) == 1){
                error_flag = 0;
                break;
            }else{
                error_flag = 1;
            }
        }
    }

    if (error_flag){
        printf("Error in receiving the ble packets.\n");
        return NULL;
    }

    /*
     *  _____  DECODE THE BUFFER  __________________________________________________
     *
     */

    DecodeBlePackets();

    /*
     *  _____  BUILD THE JSON STRING  __________________________________________________
     *
     */

    char *jsonstr = DecodeTrans41Data2Runinfo();

    // Expected Output
    //    json: {
    //        "NumberOfRuns": 4,
    //        "Run0": {
    //                "Slot": 0,
    //                "RunID":        30,
    //                "RunDataLength":        5,
    //                "ActiveDuration":       0.40300017595291138,
    //                "ActiveDuration_unit":  "minutes",
    //                "ActiveDistance":       53.468425750732422,
    //                "ActiveDistance_unit":  "m",
    //                "Calories":     5.0927219390869141,
    //                "Calories_unit":        "KCal",
    //                "AverageSpeed": 7.9605560302734375,
    //                "AverageSpeed_unit":    "Km/h",
    //                "Status":       2
    //        },
    //        "Run1": {
    //                "Slot": 1,
    //                "RunID":        38,
    //                "RunDataLength":        3,
    //                "ActiveDuration":       0.24933329224586487,
    //                "ActiveDuration_unit":  "minutes",
    //                "ActiveDistance":       32.967819213867188,
    //                "ActiveDistance_unit":  "m",
    //                "Calories":     2.9969091415405273,
    //                "Calories_unit":        "KCal",
    //                "AverageSpeed": 7.9334335327148437,
    //                "AverageSpeed_unit":    "Km/h",
    //                "Status":       2
    //        },
    //        "Run2": {
    //                "Slot": 2,
    //                "RunID":        39,
    //                "RunDataLength":        1,
    //                "ActiveDuration":       0.104166679084301,
    //                "ActiveDuration_unit":  "minutes",
    //                "ActiveDistance":       10.48656177520752,
    //                "ActiveDistance_unit":  "m",
    //                "Calories":     0.77393066883087158,
    //                "Calories_unit":        "KCal",
    //                "AverageSpeed": 6.0402588844299316,
    //                "AverageSpeed_unit":    "Km/h",
    //                "Status":       2
    //        },
    //        "Run3": {
    //                "Slot": 3,
    //                "RunID":        41,
    //                "RunDataLength":        10,
    //                "ActiveDuration":       1.0235002040863037,
    //                "ActiveDuration_unit":  "minutes",
    //                "ActiveDistance":       136.53433227539062,
    //                "ActiveDistance_unit":  "m",
    //                "Calories":     9.7040119171142578,
    //                "Calories_unit":        "KCal",
    //                "AverageSpeed": 8.0039653778076172,
    //                "AverageSpeed_unit":    "Km/h",
    //                "Status":       2
    //        }
    //    }

    /*
     *  _____  CLEAR THE DATA RELATED MEMEORY  __________________________________________________
     *
     */

    Clear_transaction_41_Data();

    /////////////////////////////////////////////////////////////////////////////////

    // DO SOMETHING WITH THE JSON STRING
    printf("json: %s", jsonstr);

    /////////////////////////////////////////////////////////////////////////////////

    return jsonstr;
}
