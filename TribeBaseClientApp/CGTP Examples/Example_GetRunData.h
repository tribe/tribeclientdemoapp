/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef EXAMPLE_BUILDBINARYFILE_H_INCLUDED
#define EXAMPLE_BUILDBINARYFILE_H_INCLUDED

#include "gtp.h"

char *Example_GetRunData();

#endif // EXAMPLE_BUILDBINARYFILE_H_INCLUDED
