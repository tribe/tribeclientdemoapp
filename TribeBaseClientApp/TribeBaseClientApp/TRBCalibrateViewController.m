//
//  TRBCalibrateViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 19/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBCalibrateViewController.h"
#import "TRBLECoordinator.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface TRBCalibrateViewController () <TRBDeviceDelegate> {
    BOOL hasStartedCalibration;
    BOOL doNotChangeState;
}

@property (nonatomic, weak) IBOutlet UIButton *startButton;
@property (nonatomic, weak) IBOutlet UIButton *stopButton;
@property (nonatomic, weak) IBOutlet UIButton *resetButton;

@end

@implementation TRBCalibrateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Calibration";
    
    [[TRBLECoordinator sharedCoordinator] connectedDevice].delegate = self;
    hasStartedCalibration = NO;
    doNotChangeState = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self prepareViewStyle:self.startButton];
    [self prepareViewStyle:self.stopButton];
    [self prepareViewStyle:self.resetButton];
    
    [self setStoppedCalibration];
}

- (void)prepareViewStyle:(UIView*)view {
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 8.0;
    view.layer.masksToBounds = YES;
}

- (IBAction)startButtonTapped:(id)sender {
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] startCalibration];
}

- (IBAction)stopButtonTapped:(id)sender {
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] stopCalibration];
}

- (IBAction)resetButtonTapped:(id)sender {
    doNotChangeState = YES;
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] resetToFactorySettings];
}

- (void)setStartedCalibration {
    hasStartedCalibration = YES;
    
    self.startButton.enabled = NO;
    self.startButton.alpha = 0.4;
    
    self.stopButton.enabled = YES;
    self.stopButton.alpha = 1.0;
}

- (void)setStoppedCalibration {
    hasStartedCalibration = NO;
    
    self.startButton.enabled = YES;
    self.startButton.alpha = 1.0;
    
    self.stopButton.enabled = NO;
    self.stopButton.alpha = 0.4;
}

#pragma mark - TRBDeviceDelegate
- (void)commandRejectedDeviceIsProcessing {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device is busy"
                                                        message:@"Command has been rejected. Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    });
}

- (void)didReadValueForType:(EEDataType)type data:(NSData *)data characteristic:(CBCharacteristic*)characteristic {
    switch (type) {
        case eeDataTypeGTP3: {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self->doNotChangeState) {
                    self->doNotChangeState = NO;
                    [SVProgressHUD showSuccessWithStatus:@"Device has been reset to factory settings."];
                    [self setStoppedCalibration];
                } else {
                    if (self->hasStartedCalibration) {
                        [self setStoppedCalibration];
                    } else {
                        [self setStartedCalibration];
                    }
                }
            });
        }
        case eeDataTypeBattery:
        case eeDataTypeGTP2:
        case eeDataTypeStartStop:
        default: {
            break;
        }
    }
}

@end
