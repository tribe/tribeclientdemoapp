//
//  RunInfoDataTableViewCell.h
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 31/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RunInfoDataTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *runIdValue;
@property (nonatomic, weak) IBOutlet UILabel *slotValue;
@property (nonatomic, weak) IBOutlet UILabel *runDataLengthValue;
@property (nonatomic, weak) IBOutlet UILabel *statusValue;

@end
