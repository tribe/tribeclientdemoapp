//
//  RunInfoDataTableViewCell.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 31/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "RunInfoDataTableViewCell.h"

@implementation RunInfoDataTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
