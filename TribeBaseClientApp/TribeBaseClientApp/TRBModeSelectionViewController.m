//
//  TRBModeSelectionViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBModeSelectionViewController.h"

@interface TRBModeSelectionViewController ()

@property (nonatomic, weak) IBOutlet UIButton *examplesButton;
@property (nonatomic, weak) IBOutlet UIButton *liveRunButton;
@property (nonatomic, weak) IBOutlet UIButton *savedRunsButton;

@end

@implementation TRBModeSelectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tribeLogoPlain"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    [self prepareViewStyle:self.examplesButton];
    [self prepareViewStyle:self.liveRunButton];
    [self prepareViewStyle:self.savedRunsButton];
}

- (void)prepareViewStyle:(UIView*)view {
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 8.0;
    view.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
