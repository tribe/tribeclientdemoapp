//
//  main.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
