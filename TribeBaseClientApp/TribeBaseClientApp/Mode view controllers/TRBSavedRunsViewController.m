//
//  TRBSavedRunsViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 29/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBSavedRunsViewController.h"
#import "TRBCGTPWrapper.h"
#import "TRBLECoordinator.h"
#import "TRBDevice.h"
#import "Example_RunInfoData.h" // Used to read run info and then get data dump from WISH, after everything has been finished
#import "Example_GetEventData.h" // Used to parse squat data on the fly.
#import "TRBDecodedDataRawViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "RunInfoDataTableViewCell.h"

@interface TRBSavedRunsViewController () <BLECoordinatorDelegate, TRBDeviceDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSMutableData *receivedData;
    NSMutableString *decodedPacketsRead;
    NSMutableArray *decodedItems;
    BOOL shouldDisconnectPeripheral;
    int currentPacketOrder;
    BOOL hasTappedStart;
    int numberOfRunsSaved;
    NSArray *savedRuns;
    NSIndexPath *selectedIndexPath;
    NSDictionary *selectedRun;
}

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIImageView *deviceImage;
@property (nonatomic, weak) IBOutlet UILabel *batteryLevel;
@property (nonatomic, weak) IBOutlet UILabel *connectionStatus;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *readListButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *deleteAllButton;

@end

@implementation TRBSavedRunsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Saved Data";
    
    [[TRBLECoordinator sharedCoordinator] configureBLEManager];
    [TRBLECoordinator sharedCoordinator].delegate = self;
    [self setSearching];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    shouldDisconnectPeripheral = YES;
    [[TRBLECoordinator sharedCoordinator] connectedDevice].delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [SVProgressHUD dismiss];
    if(shouldDisconnectPeripheral) {
        [[TRBLECoordinator sharedCoordinator] disconnectConnectedDevice];
    }
}

- (void)setSearching {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.hidden = YES;
        [self.activityIndicator startAnimating];
        self.readListButton.enabled = NO;
        self.deleteAllButton.enabled = NO;
        self.deviceImage.alpha = 0.4;
        self.connectionStatus.textColor = [UIColor lightGrayColor];
        self.connectionStatus.text = @"Searching";
    });
}

- (void)setConnected {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.hidden = NO;
        [self.activityIndicator stopAnimating];
        self.readListButton.enabled = YES;
        self.deleteAllButton.enabled = YES;
        self.deviceImage.alpha = 1.0;
        self.connectionStatus.textColor = [UIColor greenColor];
        self.connectionStatus.text = @"CONNECTED";
    });
}

- (void)prepareViewStyle:(UIView*)view {
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 8.0;
    view.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - BLECoordinatorDelegate methods
- (void)hasNotBeenConfigured {
    [[TRBLECoordinator sharedCoordinator] configureBLEManager];
    [[TRBLECoordinator sharedCoordinator] startScan];
}

- (void)bleIsAuthorized {
    [[TRBLECoordinator sharedCoordinator] startScan];
}

- (void)bleIsUnauthorizedWithState:(CBManagerState)bleAuthorizationState {
    if(bleAuthorizationState == CBManagerStateUnauthorized) {
        NSLog(@"Bluetooth is off on iPhone");
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth error"
                                                            message:@"Make sure you have authorized access to your iPhone's Bluetooth."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        });
    }
    else {
        NSLog(@"Bluetooth is off on iPhone");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth error"
                                                            message:@"Make sure Bluetooth is enabled on your iOS device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        });
    }
}

- (void)didDiscoverPeripheral:(CBPeripheral*)peripheral {
    [[TRBLECoordinator sharedCoordinator] connectToDevice];
}

- (void)didConnectPeripheral:(CBPeripheral*)peripheral {
    [peripheral discoverServices:nil];
    [TRBLECoordinator sharedCoordinator].connectedDevice.delegate = self;
    [self setConnected];
}

- (void)didDisconnectPeripheral:(CBPeripheral*)peripheral {
    [self setSearching];
    [[TRBLECoordinator sharedCoordinator] startScan];
}

#pragma mark - TRBDeviceDelegate
- (void)hasDiscoveredAllCharacteristics {
    self.connectionStatus.text = @"DISCOVERED CHARACTERISTICS";
    [SVProgressHUD showWithStatus:@"Fetching Runs List from Peripheral"];
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] readAllRunsList];
}

- (void)commandRejectedDeviceIsProcessing {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device is busy"
                                                        message:@"Command has been rejected. Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    });
}

- (void)didReadValueForType:(EEDataType)type data:(NSData*)data characteristic:(CBCharacteristic*)characteristic {
    
    switch (type) {
        case eeDataTypeBattery: {
            [self didReadBLEBatteryLevel:(uint16_t*)data.bytes];
            break;
        }
        case eeDataTypeGTP2: {
            uint8_t *dataChar = (uint8_t*)data.bytes;
            uint8_t transactionId = (uint8_t)dataChar[0];
            if (transactionId == 0x1D) {
                // Response when sending value to 0x8888 (start/stop)
                
            } else if(transactionId == 0x00) {
                uint8_t subCommand = (uint8_t)dataChar[1];
                if (subCommand == 0x21) {
                    NSMutableArray *updatedRuns = [savedRuns mutableCopy];
                    [updatedRuns removeObjectAtIndex:selectedIndexPath.row];
                    savedRuns = nil;
                    savedRuns = [updatedRuns copy];
                    selectedIndexPath = nil;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [SVProgressHUD showSuccessWithStatus:@"Data set deleted"];
                        [self.tableView reloadData];
                    });
                } else if(subCommand == 0x1F) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self->savedRuns = nil;
                        [SVProgressHUD dismiss];
                        [SVProgressHUD showSuccessWithStatus:@"All Data sets deleted"];
                        [self.tableView reloadData];
                    });
                }
            } else if(transactionId == 0x1F) {
                // Read list of all runs from the WISH
                [self didReadSavedRunsData:data withCharacteristic:characteristic];
            } else if(transactionId == 0x20) {
                [self didReceiveDataPacket:dataChar withCharacteristic:characteristic];
            }
            break;
        }
        case eeDataTypeStartStop:
        case eeDataTypeGTP3:
        default: {
            break;
        }
    }
}

#pragma mark - BLE read value methods
- (void)didReadBLEBatteryLevel:(uint16_t*)dataChar; {
    uint16_t batteryLevel = (uint16_t)dataChar[0];
    NSString *value = [NSString stringWithFormat:@"Battery level = %u%%", batteryLevel];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.text = value;
    });
}

- (void)didReadSavedRunsData:(NSData*)data withCharacteristic:(CBCharacteristic*)characteristic {
    uint8_t *dataChar = (uint8_t*)data.bytes;
    int errorFlag = 0;
    if (data) {
        if (AddPacketToBufferAndCheck(dataChar) != 1) {
            errorFlag = 1;
        }
    }
    if (errorFlag == 1) {
        NSLog(@"Error in receiving the ble packets.\n");
    } else {
        DecodeBlePackets();
        char *jsonstr = DecodeTrans41Data2Runinfo();
        Clear_transaction_41_Data();
        NSString *jsonString = [NSString stringWithUTF8String:jsonstr];
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:nil];
        savedRuns = [self getRunsFromStupidJSON:json];
        NSNumber *numberOfRuns = json[@"NumberOfRuns"];
        numberOfRunsSaved = numberOfRuns.intValue;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [self.tableView reloadData];
    });
}

- (NSArray*)getRunsFromStupidJSON:(NSDictionary*)json {
    NSMutableArray *runs = [NSMutableArray new];
    int totalRuns = ((NSNumber*)json[@"NumberOfRuns"]).intValue;
    for(int i=0; i<totalRuns; i++) {
        [runs addObject:json[[NSString stringWithFormat:@"Run%d", i]]];
    }
    return [runs copy];
}

- (void)didReceiveDataPacket:(uint8_t*)dataChar withCharacteristic:(CBCharacteristic*)characteristic {
    int lastPacketReceived = TR42_AddPacket2Buffer(dataChar);
    if (lastPacketReceived) {
        // when the last packet is received you can get a json string with an array of lost packets (if any).
        char* jsonstr = TR42_GetMissedPackets();
        NSLog(@"Missed packets: %s", jsonstr);
        
        // BinData should be written to a binary file and send to server.
        uint8_t *binaryData = 0;
        int binaryDataLength = 0;
        TR42_GetTransBuffer(&binaryData, &binaryDataLength);
        // do something with the buffer
        if (binaryData) {
            int i;
            
            char** jsonstr_array = NULL;
            
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"DataSchema" ofType:@"json"];
            FILE *fp = fopen([filePath cStringUsingEncoding: NSUTF8StringEncoding], "r");
            
            jsonstr_array = GDD_DecodeDeviceDataInOne(fp, binaryData, binaryDataLength);
            NSMutableDictionary *dict = [NSMutableDictionary new];
            int runDataLength = ((NSNumber*)self->selectedRun[@"RunDataLength"]).intValue;
            if (jsonstr_array != NULL){
                for (i=0; i < runDataLength; i++){
                    [dict setObject:[TRBCGTPWrapper convertToDictionary:jsonstr_array[i]] forKey:[NSString stringWithFormat:@"Repetition_%05d", i]];
                    printf("jsonstr_array[%d]: %s\n", i, jsonstr_array[i]);
                }
            }
            
            NSData *data = [NSData dataWithBytes:binaryData length:sizeof(uint8_t)*binaryDataLength];
            NSLog(@"Data read = %@", data.description);
            // Here we have to save this data on the current run
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [SVProgressHUD showSuccessWithStatus:@"Data downloaded"];
                [self.tableView deselectRowAtIndexPath:self->selectedIndexPath animated:YES];
                self->shouldDisconnectPeripheral = NO;
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                TRBDecodedDataRawViewController *detailController = [storyboard instantiateViewControllerWithIdentifier:@"DecodedDataViewControllerID"];
                detailController.title = @"Decoded Data set";
                detailController.results = dict;
                [self.navigationController pushViewController:detailController animated:YES];
                // clear the buffer
                free(binaryData);
                free(jsonstr_array);
                TR42_ClearMemory();
            });
        }
    }
}

#pragma mark - Table view datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return savedRuns.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *run = savedRuns[indexPath.row];
    RunInfoDataTableViewCell *cell = (RunInfoDataTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"RunInfoCellId"];
    cell.runIdValue.text = [NSString stringWithFormat:@"%d", ((NSNumber*)run[@"RunID"]).intValue];
    cell.slotValue.text = [NSString stringWithFormat:@"%d", ((NSNumber*)run[@"Slot"]).intValue];
    cell.runDataLengthValue.text = [NSString stringWithFormat:@"%d", ((NSNumber*)run[@"RunDataLength"]).intValue];
    int status = ((NSNumber*)run[@"Status"]).intValue;
    cell.statusValue.text = [NSString stringWithFormat:@"%d", status];
    if (status == 0) {
        cell.statusValue.textColor = [UIColor redColor];
    } else if(status == 1) {
        cell.statusValue.textColor = [UIColor orangeColor];
    } else if(status == 2) {
        cell.statusValue.textColor = [UIColor greenColor];
    } else {
        cell.statusValue.textColor = [UIColor purpleColor];
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Delete selected Data set?"
                                                                       message:@"Are you sure you want to delete the selected Data set? This action cannot be reverted!"
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"YES"
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction *action) {
                                                              [SVProgressHUD showWithStatus:@"Deleting Data set"];
                                                              self->selectedIndexPath = indexPath;
                                                              NSDictionary *run = self->savedRuns[indexPath.row];
                                                              int slot = ((NSNumber*)run[@"Slot"]).intValue;
                                                              [[[TRBLECoordinator sharedCoordinator] connectedDevice] deleteDataForRunWithSlot:slot];
                                                          }];
        UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"NO"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action) {
                                                         }];
        [alert addAction:noAction];
        [alert addAction:yesAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndexPath = indexPath;
    selectedRun = savedRuns[indexPath.row];
    int status = ((NSNumber*)selectedRun[@"Status"]).intValue;
    if(status == 2) {
        int slot = ((NSNumber*)selectedRun[@"Slot"]).intValue;
        [SVProgressHUD showWithStatus:@"Retrieving Data. Please do not disconnect the BLE device. This process could need approximately 2-3 minutes to complete."];
        [[[TRBLECoordinator sharedCoordinator] connectedDevice] readDataForRunWithSlot:slot];
    }
    else if (status == 1) {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Data Status issue"
                                                                       message:@"This Data set has not yet been completed. Processing the slot data is not possible, unless the Data set gets properly closed (i.e. by tapping the STOP button on the Live Data screen)."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction *action) {
                                                          }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    } else {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Data Status issue"
                                                                       message:@"Something is wrong with this Data set. Please inform us about the case."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction *action) {
                                                          }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    shouldDisconnectPeripheral = NO;
    if ([segue.identifier isEqualToString:@"DecodedDataSegueId"]) {
        TRBDecodedDataRawViewController *detailController = [segue destinationViewController];
        detailController.title = @"Total Decoded Results";
        detailController.realResults = decodedItems;
    }
}

#pragma mark - IBActions
- (IBAction)btnReadListTapped:(id)sender {
    [SVProgressHUD showWithStatus:@"Reading list"];
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] readAllRunsList];
}

- (IBAction)btnDeleteAllTapped:(id)sender {
    [SVProgressHUD showWithStatus:@"Deleting list"];
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] deleteAllRunData];
}

@end
