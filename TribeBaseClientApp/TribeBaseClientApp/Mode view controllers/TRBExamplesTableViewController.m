//
//  TRBExamplesTableViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBExamplesTableViewController.h"
#import "TRBDecodedDataRawViewController.h"
#import "TRBDocumentationViewController.h"
#import "TRBCGTPWrapper.h"
#import <MMMarkdown/MMMarkdown.h>

@interface TRBExamplesTableViewController ()

@property (nonatomic, strong) NSIndexPath *selectedIndexpath;
@property (nonatomic, strong) NSString *htmlDocument;
@property (nonatomic, strong) NSURL *baseURL;

@end

@implementation TRBExamplesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Examples";
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ExampleCellId"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if(self.selectedIndexpath) {
        [self.tableView deselectRowAtIndexPath:self.selectedIndexpath animated:YES];
        self.selectedIndexpath = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExampleCellId" forIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Example - Get Device Data";
            break;
        case 1:
            cell.textLabel.text = @"Example - Get Event Data";
            break;
        case 2:
            cell.textLabel.text = @"Example - Get Run Data";
            break;
        case 3:
            cell.textLabel.text = @"Example - Get Run Info Data";
            break;
        default:
            break;
    }
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:@"CGTPDocs" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:bundlePath];
    [bundle load];
    NSString *filePath = nil;
    switch (indexPath.row) {
        case 0: {
            
            break;
        }
        case 1: {
            filePath = [bundle pathForResource:@"README_GetEventData_v020" ofType:@"md"];
            break;
        }
        case 2: {
            
            break;
        }
        case 3: {
            filePath = [bundle pathForResource:@"README_GetRunInfoData" ofType:@"md"];
            break;
        }
        default:
            break;
    }
    if(filePath) {
        NSError *error;
        NSString *info = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        self.htmlDocument = [MMMarkdown HTMLStringWithMarkdown:info extensions:MMMarkdownExtensionsGitHubFlavored error:NULL];
        self.baseURL = [NSURL URLWithString:[filePath stringByDeletingLastPathComponent]];
        [self performSegueWithIdentifier:@"DocumentSegueId" sender:self];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexpath = indexPath;
    [self performSegueWithIdentifier:@"ExampleResultsSegueId" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"ExampleResultsSegueId"]) {
        NSString *title = nil;
        NSDictionary *json = nil;
        switch (self.selectedIndexpath.row) {
            case 0: {
                title = @"Get Device Data";
                json = [TRBCGTPWrapper exampleGetDeviceData];
                break;
            }
            case 1: {
                title = @"Get Event Data";
                json = [TRBCGTPWrapper exampleGetEventData];
                break;
            }
            case 2: {
                title = @"Get Run Data";
                json = [TRBCGTPWrapper exampleGetRunData];
                break;
            }
            case 3: {
                title = @"Get Run Info Data";
                json = [TRBCGTPWrapper exampleGetRunInfoData];
                break;
            }
            default: {
                break;
            }
        }
        TRBDecodedDataRawViewController *detailController = [segue destinationViewController];
        detailController.title = title;
        detailController.results = json;
    } else {
        TRBDocumentationViewController *documentationController = [segue destinationViewController];
        documentationController.htmlString = self.htmlDocument;
        documentationController.baseURL = self.baseURL;
    }
}

@end
