//
//  TRBLiveRunViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBLiveRunViewController.h"
#import "TRBCGTPWrapper.h"
#import "TRBLECoordinator.h"
#import "TRBDevice.h"
#import "Example_RunInfoData.h" // Used to read run info and then get data dump from WISH, after everything has been finished
#import "Example_GetEventData.h" // Used to parse squat data on the fly.
#import "TRBDecodedDataRawViewController.h"

@interface TRBLiveRunViewController () <BLECoordinatorDelegate, TRBDeviceDelegate> {
    NSMutableData *receivedData;
    NSMutableString *decodedPacketsRead;
    NSMutableArray *decodedItems;
    int currentPacketOrder;
    BOOL navigatesForward;
}

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, weak) IBOutlet UIImageView *deviceImage;
@property (nonatomic, weak) IBOutlet UILabel *batteryLevel;
@property (nonatomic, weak) IBOutlet UILabel *connectionStatus;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *calibrateButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *startButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *stopButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *switchResultsEventsButton;

@property (nonatomic, weak) IBOutlet UIView *textViewContainer;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIView *eventCounterContainer;
@property (nonatomic, weak) IBOutlet UILabel *eventCounterValue;

@end

@implementation TRBLiveRunViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Live Data";
    
    receivedData = [NSMutableData new];
    decodedPacketsRead = [NSMutableString new];
    decodedItems = [NSMutableArray new];
    currentPacketOrder = 0;
    
    [[TRBLECoordinator sharedCoordinator] configureBLEManager];
    [TRBLECoordinator sharedCoordinator].delegate = self;
    [self prepareViewStyle:self.textViewContainer];
    [self prepareViewStyle:self.eventCounterContainer];
//    self.textViewContainer.hidden = YES;
    self.eventCounterContainer.hidden = YES;
    [self setSearching];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    navigatesForward = NO;
    [[TRBLECoordinator sharedCoordinator] connectedDevice].delegate = self;
    self.eventCounterValue.text = @"-";
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(!navigatesForward) {
        [[TRBLECoordinator sharedCoordinator] disconnectConnectedDevice];
    }
}

- (void)setSearching {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.hidden = YES;
        [self.activityIndicator startAnimating];
        self.deviceImage.alpha = 0.4;
        self.calibrateButton.enabled = NO;
        self.startButton.enabled = NO;
        self.stopButton.enabled = NO;
        self.connectionStatus.textColor = [UIColor lightGrayColor];
        self.connectionStatus.text = @"Searching";
    });
}

- (void)setConnected {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.hidden = NO;
        [self.activityIndicator stopAnimating];
        self.deviceImage.alpha = 1.0;
        self.connectionStatus.textColor = [UIColor greenColor];
        self.connectionStatus.text = @"CONNECTED";
    });
}

- (void)prepareViewStyle:(UIView*)view {
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 8.0;
    view.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startButtonTapped {
    NSLog(@"Start tapped");
    NSNumber *weight = @(80);
    NSNumber *height = @(180);
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] setWeight:(uint16_t)[weight unsignedLongValue] height:(uint16_t)[height unsignedLongValue]];
}

- (IBAction)stopButtonTapped {
    NSLog(@"Stop tapped");
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] stopRun];
}

- (void)scrollTextViewToBottom:(UITextView *)textView {
    CGSize size = CGSizeMake(self.textView.frame.size.width, self.textView.contentSize.height);
    [textView scrollRectToVisible:CGRectMake(0, size.height - 100, size.width, 100) animated:YES];
    textView.scrollEnabled = NO;
    textView.scrollEnabled = YES;
}

#pragma mark - BLECoordinatorDelegate methods
- (void)hasNotBeenConfigured {
    [[TRBLECoordinator sharedCoordinator] configureBLEManager];
    [[TRBLECoordinator sharedCoordinator] startScan];
}

- (void)bleIsAuthorized {
    [[TRBLECoordinator sharedCoordinator] startScan];
}

- (void)bleIsUnauthorizedWithState:(CBManagerState)bleAuthorizationState {
    if(bleAuthorizationState == CBManagerStateUnauthorized) {
        NSLog(@"Bluetooth is off on iPhone");
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth error"
                                                            message:@"Make sure you have authorized access to your iPhone's Bluetooth."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        });
    }
    else {
        NSLog(@"Bluetooth is off on iPhone");

        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bluetooth error"
                                                            message:@"Make sure Bluetooth is enabled on your iOS device"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        });
    }
}

- (void)didDiscoverPeripheral:(CBPeripheral*)peripheral {
    [[TRBLECoordinator sharedCoordinator] connectToDevice];
}

- (void)didConnectPeripheral:(CBPeripheral*)peripheral {
    [peripheral discoverServices:nil];
    [TRBLECoordinator sharedCoordinator].connectedDevice.delegate = self;
    [self setConnected];
}

- (void)didDisconnectPeripheral:(CBPeripheral*)peripheral {
    [self setSearching];
    [[TRBLECoordinator sharedCoordinator] startScan];
}

#pragma mark - TRBDeviceDelegate
- (void)hasDiscoveredAllCharacteristics {
    self.connectionStatus.text = @"DISCOVERED CHARACTERISTICS";
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] readStartStopState];
}

- (void)hasUpdatedBodyMeasurements {
    [[[TRBLECoordinator sharedCoordinator] connectedDevice] startRun];
}

- (void)hasStarted {
    self.connectionStatus.text = @"DEVICE HAS STARTED";
    self.startButton.enabled = NO;
    self.stopButton.enabled = YES;
    self.calibrateButton.enabled = NO;
    self.textView.text = @"";
    receivedData = [NSMutableData new];
    decodedPacketsRead = [NSMutableString new];
    decodedItems = [NSMutableArray new];
    self.eventCounterValue.text = @"-";
}

- (void)hasStopped {
    self.connectionStatus.text = @"DISCOVERED CHARACTERISTICS";
    self.startButton.enabled = YES;
    self.stopButton.enabled = NO;
    self.calibrateButton.enabled = YES;
}

- (void)commandRejectedDeviceIsProcessing {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Device is busy"
                                                        message:@"Command has been rejected. Please try again."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    });
}

- (void)didReadValueForType:(EEDataType)type data:(NSData*)data characteristic:(CBCharacteristic*)characteristic {
    switch (type) {
        case eeDataTypeBattery: {
            [self didReadBLEBatteryLevel:(uint16_t*)data.bytes];
            break;
        }
        case eeDataTypeGTP2: {
            uint8_t *dataChar = (uint8_t*)data.bytes;
            uint8_t transactionId = (uint8_t)dataChar[0];
            if (transactionId == 0x1D) {
                // Response when sending value to 0x8888 (start/stop)
            } else if(transactionId == 0x03) {
                [self didReadLiveData:data withCharacteristic:characteristic];
            }
            break;
        }
        case eeDataTypeIMUBasic: {
            [self didReadIMUBasicLiveData:data];
            break;
        }
        case eeDataTypeStartStop: {
            uint16_t *dataChar = (uint16_t *)data.bytes;
            uint16_t startStop = (uint16_t)dataChar[0];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (startStop == 1) {
                    [self hasStarted];
                } else {
                    [self hasStopped];
                }
            });
            break;
        }
        case eeDataTypeGTP3:
        default: {
            break;
        }
    }
}

#pragma mark - BLE read value methods
- (void)didReadBLEBatteryLevel:(uint16_t*)dataChar; {
    uint16_t batteryLevel = (uint16_t)dataChar[0];
    NSString *value = [NSString stringWithFormat:@"Battery level = %u%%", batteryLevel];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.batteryLevel.text = value;
    });
}

- (void)didReadLiveData:(NSData*)data withCharacteristic:(CBCharacteristic*)characteristic {
    // Read each incoming packet -> Real time
    uint8_t *dataChar = (uint8_t*)data.bytes;
    NSString *separator = @"\n------------------------------------------------------------------------";
    NSString *characteristicString = [NSString stringWithFormat:@"\n\n### Packet %d received in TRBConnectionViewController ###\n\nCharacteristic value: \n\n%@\n\n", currentPacketOrder, characteristic.description];
    NSString *dataBytes = [NSString stringWithFormat:@"\n\n###\n\nData bytes: %@\n\n", data.description];
    NSLog(@"%@%@%@%@%@%@%@%@%@", separator, separator, separator, characteristicString, separator, dataBytes, separator, separator, separator);
    FILE *fp = [TRBCGTPWrapper dataschemaFilePointer];
    InitializeWithDataSchema(fp);
    char *decoded = DecodeWithPacket(dataChar);
    NSDictionary *JSON = @{};
    if(decoded != NULL) {
        currentPacketOrder = 0;
        JSON = [TRBCGTPWrapper convertToDictionary:decoded];
        [decodedItems addObject:JSON];
        if(decodedPacketsRead.length > 0) {
            [decodedPacketsRead appendFormat:@"\n\n%@", JSON.description];
        } else {
            [decodedPacketsRead appendString:JSON.description];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self->_textView.text = self->decodedPacketsRead;
            [self scrollTextViewToBottom:self.textView];
        });
    } else {
        currentPacketOrder ++;
    }
    [receivedData appendData:data];
}

- (void)didReadIMUBasicLiveData:(NSData*)data {
    float *dataChar = (float*)data.bytes;
    float step = (float)dataChar[0];
//    float duration = (float)dataChar[1];
//    float distance = (float)dataChar[2];
//    float speed = (float)dataChar[3];
//    float someVal = (float)dataChar[4];
//    float calories = (float)dataChar[5];
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_eventCounterValue.text = [NSString stringWithFormat:@"%.0f", step];
    });
}

#pragma mark - IBActions
- (IBAction)btnResultsEventsTapped:(id)sender {
//    self.eventCounterContainer.hidden = !self.eventCounterContainer.hidden;
//    self.textViewContainer.hidden = !self.textViewContainer.hidden;
    if(self.eventCounterContainer.hidden) {
        self.switchResultsEventsButton.title = @"EVENTS";
    } else {
        self.switchResultsEventsButton.title = @"RESULTS";
    }
}

#pragma mark - Navigation
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     navigatesForward = YES;
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }


@end
