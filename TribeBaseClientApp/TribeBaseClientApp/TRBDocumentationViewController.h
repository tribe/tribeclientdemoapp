//
//  TRBDocumentationViewController.h
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 02/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRBDocumentationViewController : UIViewController

@property (nonatomic, strong) NSString *htmlString;
@property (nonatomic, strong) NSURL *baseURL;
@end
