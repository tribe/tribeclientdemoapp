//
//  TRBDocumentationViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 02/07/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBDocumentationViewController.h"

@interface TRBDocumentationViewController ()

@property (nonatomic, weak) IBOutlet UIWebView *webview;

@end

@implementation TRBDocumentationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Documentation";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.webview loadHTMLString:self.htmlString baseURL:self.baseURL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
