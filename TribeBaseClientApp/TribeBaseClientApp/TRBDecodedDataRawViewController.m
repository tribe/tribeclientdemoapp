//
//  TRBDecodedDataRawViewController.m
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import "TRBDecodedDataRawViewController.h"

@interface TRBDecodedDataRawViewController ()

@property (nonatomic, weak) IBOutlet UIView *textViewContainer;
@property (nonatomic, weak) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *shareButtonItem;

@end

@implementation TRBDecodedDataRawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareViewStyle:self.textViewContainer];
    if (self.results) {
        self.textView.text = self.results.description;
    } else if (self.realResults) {
        self.textView.text = self.realResults.description;
    } else {
        self.textView.text = @"No text results exist for this case";
    }
    self.textView.contentOffset = CGPointMake(0, 0);
}

- (void)prepareViewStyle:(UIView*)view {
    view.layer.borderColor = [UIColor darkGrayColor].CGColor;
    view.layer.borderWidth = 1.0;
    view.layer.cornerRadius = 8.0;
    view.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)shareButtonTapped {
    if(!self.results && !self.realResults) {
        return;
    }
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:@[self.realResults ? self.realResults.description : self.results.description] applicationActivities:nil];
    activityController.popoverPresentationController.sourceView = self.view;
    [self presentViewController:activityController animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
