//
//  TRBDecodedDataRawViewController.h
//  TribeBaseClientApp
//
//  Created by Petros Sfikakis on 30/06/2018.
//  Copyright © 2018 Tribe Private Company. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TRBDecodedDataRawViewController : UIViewController

@property (nonatomic, strong) NSDictionary *results;
@property (nonatomic, strong) NSArray *realResults;

@end
