# CGTP Documentation

## Overview

The recent increase in interest in wireless communication with edge devices (IoT, Wearables, etc.) has forced developers in this space to devote a lot of their time just for develop and maintenance of that communication.

Such networks are by nature very dynamic: the wireless links may temporarily break at any time, and the types of the communicated data can change with an update. In such situations, the conventional approach of using addresses, characteristics, and channels for communicating
over standard protocols (e.g., Bluetooth LE) with the individual nodes may become a nightmare to develop and maintain.

Tribe has developed a simple two-way C-based General Transfer Protocol (CGTP) to communicate with edge devices over Bluetooth.



## Benefits

The [CGTP](#cgtp-c-based-version-of-gtp-library) protocol library has been developed with simplicity, performance, and ease-of-use in mind. The library operates atop the Bluetooth protocol, utilizing only one service/characteristic to retrieve the info from the device and outputs a [JSON](#json-javascript-object-notation) file with all retrieved data.

[CGTP](#cgtp-c-based-version-of-gtp-library) benefits:
- improve wireless communication stability (minimize wake/sleep time)
- accelerate development: reduce the number of services/characteristics in need of development & maintenance
- plug-and-play Installation and Update in the form of a C-based library



## Installation Guide

To install the [CGTP](#cgtp-c-based-version-of-gtp-library) library, you have to import into your project the:

1. static library file: *_LibCGTP.a_*
2. header files *.h: included in the _CGTP_ folder
3. *DataSchema.json* file (import it in same folder as the static library)

> ***@PETROS*** & **@JOHN**: HERE YOU CAN DESCRIBE SOME THINGS REGARDING THE IOS/ANDROID AND HOW TO IMPORT THE STATIC LIBRARY, THE HEADER FILES AND THE *DataSchema.json* TO THE RIGHT PLACE.



## Code Examples

The following example presents how the [CGTP](#cgtp-c-based-version-of-gtp-library) library can be used to get various metrics per **event** from TWD (Tribe's Wearable Device).

> **event**: an instance of any repeated action like a step, squat, lift, etc.



###   Example 1: Get Current Event Data

After successfully following the [Insallation Guide](#installation-guide), you can access the library by including the *gtp.h* file into your code:

```C
#include "CGTP/gtp.h"
```



#### Check BLE Packets and Build the Buffer

Use the function
```c
int TR3_AddPacketToBufferAndCheck(uint8_t* packet);
```
 to upend incoming [BLE](#ble-bluetooth-low-energy)  packets from [TWD](#twd-tribes-wearable-device). The function will return the integer one (1) once all the necessary packets for the construction of the JSON export are received.

```c
#include <stdint.h>

int TR3_AddPacketToBufferAndCheck(uint8_t* packet){
    // pass BLE packet bytes to the buffer
    if ( AddPacketDataToTransBuffer(packet) < 0 ){
        printf("Error at packet %d.\n", 1);
        return -1;
    }

    // check if ALL the ble packets received successfully.
    if ( CheckReceivedPackets() < 0 ){
         printf("Some packets may have been lost or did not received yet.\n");
         return -1;
    }
    else{
        // call the following function to check the Buffer.
        if ( CheckTransBuffer() < 0){
            printf("The Buffer has NOT been created successfully.\n");
            return -1;
        }else{
            printf("The Buffer has been created successfully.\n");
            return 1;
        }
    }

    return 0;
}
```

Internal functions of the [CGTP](#cgtp-c-based-version-of-gtp-library) library:

* `AddPacketDataToTransBuffer(packet)`
* `CheckReceivedPackets()`
* `CheckTransBuffer()`



#### Decode the Buffer

The next step is to decode the produced buffer that has been created from the combination of the three BLE packets. The following function does exactly that; that is, decodes and access the data as well as clears memory that has been allocated during this process.

```c
void TR3_DecodeBlePackets(){
    Init_Transaction_3_Structures();

    Decoder( TR3_DataContainer,
             (uint32_t)TR3_ContainerLength,
             Transaction_3_SchemaDescription );

    Wrap_Transaction_3_Structures();
    Clear_Transaction_3_Structures();
}
```

Note that the functions:

- `Init_Transaction_3_Structures()`
  - initializes various variables that will be used to get the decoded values.
- `Decoder(...)`
  - this is the heart of the [CGTP](#cgtp-c-based-version-of-gtp-library) library and decodes any properly created byte stream.
- `Wrap_Transaction_3_Structures()`
  - assigns the decoded data to the corresponding variables.
- `Clear_Transaction_3_Structures()`
  - clears any memory that has been allocated during the decoding process.

as well as the variables:

* `TR3_DataContainer`
* `TR3_ContainerLength`
* `Transaction_3_SchemaDescription`

are internal entities of the [CGTP](#cgtp-c-based-version-of-gtp-library) library.

`TR3_DecodeBlePackets() ` is called after getting a proper output (i.e. the value 1) from `TR3_AddPacketToBufferAndCheck(packet)`. In fact, that is all you need to start getting measurements from TWD.



#### Read the JSON file

This corresponding function is used in order to read the json file *DataSchema.json*. This file is used in order "inform" the [CGTP](#cgtp-c-based-version-of-gtp-library) about what metric we want to get as output.

```c
#define BUF 8192
char DataSchema[BUF];

void GetDataSchema(){
    int i;
    size_t nread;

    // Read the data schema from the json file DataSchema.json
    for (i=0; i<BUF; i++){
        DataSchema[i] = '\0';
    }

    FILE *fp;
    fp = fopen("DataSchema.json", "r");
    if (fp == NULL){
        printf("Could not open file.");
    }else{
        while ((nread = fread(DataSchema, BUF, sizeof(char), fp)) > 0){}
    }
    fclose(fp);
}
```

An indicating example of the *DataSchema.json* file would be the follow:

```json
{
  "numberOfMetrics": 30,
  "metric0": "Event_Counter", 
  "metric1": "Event_Position", 
  "metric2": "Event_Duration",
  "metric3": "Event_Frequency",
  "metric4": "Total_Duration",
  "metric5": "Active_Duration",
  "metric6": "Intensity_Right_GMed",
  "metric7": "Intensity_Right_GM",
  "metric8": "Intensity_Right_BF",
  "metric9": "Intensity_Right_VLVM",
  "metric10": "Intensity_Right_Add",
  "metric11": "Intensity_Right_RF",
  "metric12": "Intensity_Left_GMed",
  "metric13": "Intensity_Left_GM",
  "metric14": "Intensity_Left_BF",
  "metric15": "Intensity_Left_VLVM",
  "metric16": "Intensity_Left_Add",
  "metric17": "Intensity_Left_RF",
  "metric18": "Symmetry_Right_GMed",
  "metric19": "Symmetry_Right_GM",
  "metric20": "Symmetry_Right_BF",
  "metric21": "Symmetry_Right_VLVM",
  "metric22": "Symmetry_Right_Add",
  "metric23": "Symmetry_Right_RF",
  "metric24": "Symmetry_Left_GMed",
  "metric25": "Symmetry_Left_GM", 
  "metric26": "Symmetry_Left_BF",
  "metric27": "Symmetry_Left_VLVM",
  "metric28": "Symmetry_Left_Add",
  "metric29": "Symmetry_Left_RF"
}
```

The above JSON structure is based on the following protocol:

##### *DataSchema.json* build protocol

* the first field in named "**numberOfMetrics**" and contains the number of the metrics we want to get from the TWD.
* all the following fields are based on the pattern: `"metric<Idx>": <Metric Name>,`
* the `<Idx>` **must** start from 0 and increase its value with step 1.



#### Integration

At this point we will put the aforementioned functions together. Let's first suppose that there is a loop where BLE packets are received continuously. Inside this loop, the ` TR3_AddPacketToBufferAndCheck(packet)` manages  the received packets and returns 1 only when three consecutive BLE packets combined together successfully. Afterwards, the `TR3_DecodeBlePackets()`  is used to decode the created buffer. The next step, after having decoded the buffer, is to get the TWD output. This can be achieved by using the   `GetTrans3Data(DataSchema)` function, which returns a pointer (`char*`) of the a json-based string. This string <u>must be parsed with any json parser to access the TWD output</u>. Finally, we have to clear the memory that has been allocated, by using the `Clear_Transaction_3_Data()` function, in order to repeat the process again for the next three BLE packets.

```C
void Example_GetEventData(){
	// Read the data schema from the json file DataSchema.json
    GetDataSchema();
    
	// SOME BLE RECEIVAL LOOP STARTS HERE
    
        // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        uint8_t* packet = //<A FUNCTION THAT RETURNS THE RECEIVED PACKET GOES HERE>
        // -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
        
        // check the packet's pointer
        if (packet){
            // add the packet to the buffer and check if all 
            // the packets have been received successfully.
            if ( TR3_AddPacketToBufferAndCheck(packet) == 1){
			   //  _____  DECODE THE BUFFER  _____
    		   TR3_DecodeBlePackets();
                //  _____  BUILD THE JSON STRING _____
    		   char* jsonstr = GetTrans3Data(DataSchema);
                //  _____  CLEAR THE DATA RELATED MEMEORY  _____
    		   Clear_Transaction_3_Data();
                //  _____ DO SOMETHING WITH THE JSON STRING  _____
    		   printf("json: %s", jsonstr);
            }
        }
    
    // THE BLE RECEIVAL LOOP ENDS HERE            
}
```



#### Resulted Output 

Let's suppose that three consecutive BLE packets, that bear *dummy* data, have the following values:

```C
// BLE packet 1
{3, 1, 3, 177, 0, 0, 0, 236, 67, 0, 0, 112, 66, 0, 34, 33, 71, 92, 143, 194, 62, 202, 107, 40, 64, 144, 212, 59, 63, 206, 148, 46, 63, 13, 229, 29, 67, 45, 107, 19, 65, 129, 171, 35, 66, 129, 171, 35, 66, 58, 180, 8, 63, 38, 6, 193, 62, 62, 110, 222, 64, 172, 100, 113, 63, 162, 138, 25, 191, 171, 179, 24, 62, 141, 10, 225, 55, 179, 213, 14, 59, 69, 180, 9, 64, 9, 216, 109, 58, 98, 248, 227, 60, 158, 224, 50, 64, 147, 71, 40, 64, 156, 38, 138, 192, 20, 132, 1, 65, 25, 160, 7, 64, 166, 240, 77, 64, 113, 250, 210, 63, 97, 177, 233, 64, 113, 97, 192, 62, 20, 30, 51, 64, 73, 70, 57, 63, 98, 194, 44, 63, 51, 236, 39, 67, 236, 168, 22, 65, 175, 85, 42, 62, 40, 131, 22, 57, 202, 218, 35, 59, 194, 222, 119, 62, 105, 87, 78, 62, 78, 248, 149, 62, 178, 169, 220, 62, 229, 208, 32, 63, 11},
    
// BLE packet 2
{3, 2, 3, 177, 0, 232, 133, 62, 233, 243, 243, 61, 224, 176, 199, 61, 153, 163, 12, 62, 183, 149, 17, 62, 21, 174, 255, 61, 0, 236, 249, 61, 155, 201, 251, 61, 242, 253, 212, 61, 3, 77, 31, 62, 214, 222, 147, 62, 34, 219, 0, 63, 22, 218, 14, 62, 212, 214, 68, 66, 202, 141, 65, 66, 107, 142, 59, 66, 199, 243, 3, 66, 58, 253, 158, 65, 163, 163, 58, 66, 44, 41, 75, 66, 54, 114, 78, 66, 149, 113, 84, 66, 28, 6, 134, 66, 177, 64, 160, 66, 93, 92, 85, 66, 241, 212, 45, 63, 132, 68, 124, 63, 0, 0, 62, 66, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 82, 213, 178, 62, 252, 106, 150, 62, 88, 179, 192, 62, 226, 158, 91, 63, 150, 95, 101, 63, 189, 182, 240, 62, 164, 7, 22, 62, 130, 216, 218, 61, 127, 145, 43, 62, 238, 17, 201, 62, 79, 87, 130, 62, 96, 123},
    
// BLE packet 3
{3, 3, 3, 118, 0, 72, 62, 2, 163, 79, 62, 184, 105, 63, 62, 50, 213, 85, 62, 215, 43, 238, 62, 237, 51, 36, 63, 14, 121, 140, 62, 171, 254, 47, 66, 227, 4, 26, 66, 182, 30, 52, 66, 14, 242, 44, 66, 29, 16, 216, 65, 49, 243, 40, 66, 85, 1, 96, 66, 27, 251, 117, 66, 76, 225, 91, 66, 241, 13, 99, 66, 249, 251, 145, 66, 207, 12, 103, 66, 253, 105, 164, 63, 74, 131, 120, 63, 3, 123, 82, 62, 253, 253, 52, 64, 66, 54, 80, 62, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
```

Then these packets will produce a json-based string output like the following one:

```json
json: {
        "Event_Counter":        60,
        "Event_Position":       41250,
        "Event_Duration":       0.37999999523162842,
        "Event_Frequency":      2.6315789222717285,
        "Total_Duration":       0.534000039100647,
        "Active_Duration":      0.37700003385543823,
        "Intensity_Right_GMed": 0.11911756545305252,
        "Intensity_Right_GM":   0.097505331039428711,
        "Intensity_Right_BF":   0.13734282553195953,
        "Intensity_Right_VLVM": 0.1421726793050766,
        "Intensity_Right_Add":  0.12484375387430191,
        "Intensity_Right_RF":   0.12203216552734375,
        "Intensity_Left_GMed":  0.12294312566518784,
        "Intensity_Left_GM":    0.10399998724460602,
        "Intensity_Left_BF":    0.15556721389293671,
        "Intensity_Left_VLVM":  0.28880947828292847,
        "Intensity_Left_Add":   0.50334370136260986,
        "Intensity_Left_RF":    0.13950380682945251,
        "Symmetry_Right_GMed":  49.209793090820312,
        "Symmetry_Right_GM":    48.388465881347656,
        "Symmetry_Right_BF":    46.889080047607422,
        "Symmetry_Right_VLVM":  32.988063812255859,
        "Symmetry_Right_Add":   19.873645782470703,
        "Symmetry_Right_RF":    46.6598014831543,
        "Symmetry_Left_GMed":   50.790206909179688,
        "Symmetry_Left_GM":     51.611534118652344,
        "Symmetry_Left_BF":     53.110919952392578,
        "Symmetry_Left_VLVM":   67.011932373046875,
        "Symmetry_Left_Add":    80.126350402832031,
        "Symmetry_Left_RF":     53.3401985168457
}
```



##### Naming Conventions Of the Output

The naming convention of the aforementioned *INTENSITY* and *SYMMETRY* metrics follow the pattern: 

`<FeatureName>_<Side>_<MuscleName>`

where the *FeatureName* can be either:

* `INTENSITY` : a measure of the average effort per muscle
* `SYMMETRY` : an indicator of the of muscle effort distribution per leg

the *Side* can be: 

* `Left` : left leg
* `Right` : right leg



![](upper-body-muscle-anatomy-anatomy.jpg)
and finally, the *MuscleName* can be one of the:

* `GMed` :  Gluteus Medius
* `GM` : Gluteus Maximus
* `BF` : Biceps Femoris
* `VLVM` : Vastus Lateralis, Vastus Medialis
* `Add` : Adductor
* `RF` : Rectus Femoris



#### Units

* **Event_Counter**: dimensionless (1)
  * number of event repetitions
* **Event_Position**: dimensionless (1)
  * just an index
* **Event_Duration**: seconds (sec)
* **Event_Frequency**: events/second (Hz)
* **Total_Duration**: minutes (min)
* **Active_Duration**: minutes (min)
* **Intensity\_<Side>\_<MuscleName>**: dimensionless (1)
  * due to a normalization process
* **Symmetry\_<Side>\_<MuscleName>**: percentage (%)



## Glossary

##### CGTP: C-based version of GTP library

##### TWD: Tribe's Wearable Device

##### BLE: Bluetooth Low Energy

##### JSON: JavaScript Object Notation
