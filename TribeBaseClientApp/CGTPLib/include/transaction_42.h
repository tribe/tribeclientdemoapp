/*
 *  =============================================
 *
 *  Copyright Tribe Private Company, 2018
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Private Company.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

 /*
 *  The transaction_42 executed in the MOBILE phone
 *  and is responsible for receiving, decoding and concatenating WISH data
 *  that are being sent through the transaction_32 (the complete run data that have been
 *  saved in the WISH flash memory).
 */

#ifndef TRANSACTION_42_H_INCLUDED
#define TRANSACTION_42_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "gtp_buffer.h"
#include "cJSON.h"

// extern BUFFER TR42_Buf;

extern cGTP_UINT32 TR42_PacketHeader_RunDataLength;

int TR42_AddPacket2Buffer(cGTP_UINT8* PacketPtr);
char* TR42_GetMissedPackets();
int TR42_BuildTheBinaryFile(char* filePath);
int TR42_GetTransBuffer(uint8_t** DestBuffer, int* BufferLength);
void TR42_ClearMemory();

#endif // TRANSACTION_42_H_INCLUDED
