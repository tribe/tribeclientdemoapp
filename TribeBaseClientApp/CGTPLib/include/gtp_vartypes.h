/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */



#ifndef VARTYPES_H_INCLUDED
#define VARTYPES_H_INCLUDED

#include <stdint.h>

typedef void* cGTP_OBJECT_ptr;

typedef char     cGTP_CHAR;          //     1  byte
typedef unsigned char   cGTP_UCHAR;  //     1  byte

typedef int8_t   cGTP_INT8;          //     1  byte
typedef int16_t  cGTP_INT16;         //     2  bytes
typedef int32_t  cGTP_INT32;         //     4  bytes
typedef int64_t  cGTP_INT64;         //     8  bytes

typedef uint8_t  cGTP_UINT8;         //     1  byte
typedef uint16_t cGTP_UINT16;        //     2  bytes
typedef uint32_t cGTP_UINT32;        //     4  bytes
typedef uint64_t cGTP_UINT64;        //     8  bytes

typedef float    cGTP_SINGLE;        //     4  bytes
typedef double   cGTP_DOUBLE;        //     8  bytes

typedef enum cGTP_BOOL
{
    cGTP_FALSE = 0,
    cGTP_TRUE
} cGTP_BOOL;

typedef enum cGTP_DATA_TYPES
{
    cGTP_dt_NULL = 0,
    cGTP_dt_Int8 = 11,
    cGTP_dt_Uint8,
    cGTP_dt_Int16,
    cGTP_dt_Uint16,
    cGTP_dt_Int32,
    cGTP_dt_Uint32,
    cGTP_dt_Int64,
    cGTP_dt_Uint64,
    cGTP_dt_Single,
    cGTP_dt_Double,
    cGTP_dt_Char
} cGTP_DATA_TYPES;


/**
 * @brief Macro that returns the size of each used data type
 *
 */
#define GetDataTypeSize(dtType)  ( dtType == cGTP_dt_Single ? sizeof(cGTP_SINGLE) : \
                                 ( dtType == cGTP_dt_Uint8  ? sizeof(cGTP_UINT8)  : \
                                 ( dtType == cGTP_dt_Uint16 ? sizeof(cGTP_UINT16) : \
                                 ( dtType == cGTP_dt_Uint32 ? sizeof(cGTP_UINT32) : \
                                 ( dtType == cGTP_dt_Uint64 ? sizeof(cGTP_UINT64) : \
                                 ( dtType == cGTP_dt_Int8   ? sizeof(cGTP_INT8)   : \
                                 ( dtType == cGTP_dt_Int16  ? sizeof(cGTP_INT16)  : \
                                 ( dtType == cGTP_dt_Int32  ? sizeof(cGTP_INT32)  : \
                                 ( dtType == cGTP_dt_Int64  ? sizeof(cGTP_INT64)  : \
                                 ( dtType == cGTP_dt_Double ? sizeof(cGTP_DOUBLE) : \
                                 ( dtType == cGTP_dt_NULL   ? 0              : 0 )))))))))))

#define GetDataTypePtr1DSize(dtType) ( dtType == cGTP_dt_Single ? sizeof(cGTP_SINGLE*) : \
                                     ( dtType == cGTP_dt_Uint8  ? sizeof(cGTP_UINT8*)  : \
                                     ( dtType == cGTP_dt_Uint16 ? sizeof(cGTP_UINT16*) : \
                                     ( dtType == cGTP_dt_Uint32 ? sizeof(cGTP_UINT32*) : \
                                     ( dtType == cGTP_dt_Uint64 ? sizeof(cGTP_UINT64*) : \
                                     ( dtType == cGTP_dt_Int8   ? sizeof(cGTP_INT8*)   : \
                                     ( dtType == cGTP_dt_Int16  ? sizeof(cGTP_INT16*)  : \
                                     ( dtType == cGTP_dt_Int32  ? sizeof(cGTP_INT32*)  : \
                                     ( dtType == cGTP_dt_Int64  ? sizeof(cGTP_INT64*)  : \
                                     ( dtType == cGTP_dt_Double ? sizeof(cGTP_DOUBLE*) : \
                                     ( dtType == cGTP_dt_NULL   ? 0              : 0 )))))))))))

typedef enum cGTP_STRUCTURE_TYPES
{
    cGTP_st_Element = 101,
    cGTP_st_Array,
    cGTP_st_Cell
} cGTP_STRUCTURE_TYPES;


typedef enum cGTP_BUFFER_CATEGORIES
{
    cGTP_bd_Buffer,
    cGTP_bd_BleBuffer
} cGTP_BUFFER_CATEGORIES;

#define BUFFER_SIZE 1024

#define BUFFER_TYPE cGTP_UINT8
#define BLE_PACKET_TYPE cGTP_UINT8

//#define REAL_TYPE_DATA
#ifndef REAL_TYPE_DATA

    #define BLE_PACKET_SIZE 182
    #define BLE_PACKET_DATA_SIZE 177
    #define BLE_PACKET_HEADER_SIZE 5

    typedef cGTP_UINT8 HEADER_STR_TYPE;
    #define HEADER_STR_TYPE_TYPE cGTP_dt_Uint8
    typedef cGTP_UINT8 HEADER_DIM_TYPE;
    #define HEADER_DIM_TYPE_TYPE cGTP_dt_Uint8
    typedef cGTP_UINT16 HEADER_SHP_TYPE;
    #define HEADER_SHP_TYPE_TYPE cGTP_dt_Uint16
    typedef cGTP_UINT8 HEADER_DAT_TYPE;
    #define HEADER_DAT_TYPE_TYPE cGTP_dt_Uint8

    typedef cGTP_UINT8 BLE_HEADER_TRID_TYPE;
    #define BLE_HEADER_TRID_TYPE_TYPE cGTP_dt_Uint8
    typedef cGTP_UINT8 BLE_HEADER_PACn_TYPE;
    #define BLE_HEADER_PACn_TYPE_TYPE cGTP_dt_Uint8
    typedef cGTP_UINT8 BLE_HEADER_ONOP_TYPE;
    #define BLE_HEADER_ONOP_TYPE_TYPE cGTP_dt_Uint8
    typedef cGTP_UINT16 BLE_HEADER_PLIB_TYPE;
    #define BLE_HEADER_PLIB_TYPE_TYPE cGTP_dt_Uint16

#else

    #define BLE_PACKET_SIZE 182
    #define BLE_PACKET_DATA_SIZE 166
    #define BLE_PACKET_HEADER_SIZE 16

    typedef cGTP_SINGLE BLE_HEADER_TRID_TYPE;
    #define BLE_HEADER_TRID_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE BLE_HEADER_PACn_TYPE;
    #define BLE_HEADER_PACn_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE BLE_HEADER_ONOP_TYPE;
    #define BLE_HEADER_ONOP_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE BLE_HEADER_PLIB_TYPE;
    #define BLE_HEADER_PLIB_TYPE_TYPE cGTP_dt_Single

    typedef cGTP_SINGLE HEADER_STR_TYPE;
    #define HEADER_STR_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE HEADER_DIM_TYPE;
    #define HEADER_DIM_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE HEADER_SHP_TYPE;
    #define HEADER_SHP_TYPE_TYPE cGTP_dt_Single
    typedef cGTP_SINGLE HEADER_DAT_TYPE;
    #define HEADER_DAT_TYPE_TYPE cGTP_dt_Single
#endif // REAL_TYPE_DATA

#endif // VARTYPES_H_INCLUDED
