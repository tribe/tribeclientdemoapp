/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */


#ifndef GTP_H_INCLUDED
#define GTP_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
//#include "gtp_vartypes.h"
//#include "GtpProtocol/gtp_protocolinfo.h"
//#include "Transactions/transaction_1.h"
//#include "gtp_buffer.h"
//#include "gtp_blepacket.h"
//#include "gtp_utilities.h"
//#include "gtp_structures.h"
#include "gtp_encoder.h"
#include "gtp_decoder.h"
#include "cJSON.h"
#include "transaction_41.h"
#include "transaction_42.h"
#include "transaction_3.h"

#endif // GTP_H_INCLUDED
