/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */


#ifndef BUFFER_H_INCLUDED
#define BUFFER_H_INCLUDED

#include "gtp_vartypes.h"
#include <stdlib.h>
#include <string.h>

/*
 *  _____  Buffer  __________________________________________________
 *
 */

 // General case

typedef struct BUFFER
{
    BUFFER_TYPE *Bytes;
    cGTP_UINT32 BufferIdx;
    cGTP_UINT32 Length;
    cGTP_UINT32 MaxIdx;
    //cGTP_UINT8  Mode;
} BUFFER;

// used for the simultaneous definition and initialization the above structures
#define DefBUFFER(Name) BUFFER Name = {.BufferIdx = NULL, .BufferIdx = 0, .Length = 0}

cGTP_INT8 Init_BUFFER (BUFFER* InputObjref, cGTP_UINT32 Length);
cGTP_INT8 Init_BUFFER_Wrap_Memory (BUFFER* InputObjref, BUFFER_TYPE* Bytes, cGTP_UINT32 Length);
cGTP_INT8 Clear_BUFFER(BUFFER* InputObjref);

cGTP_INT8   bseek (BUFFER* Objref, cGTP_UINT32 newPos);
cGTP_UINT32 btell (BUFFER* Objref);
cGTP_INT8   bwrite(BUFFER* Objref, cGTP_OBJECT_ptr SrcVal, cGTP_DATA_TYPES dtType);
cGTP_INT8   bread (BUFFER* Objref, cGTP_OBJECT_ptr DestVal, cGTP_DATA_TYPES dtType);

#endif // BUFFER_H_INCLUDED
