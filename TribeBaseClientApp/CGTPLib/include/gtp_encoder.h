/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

/*
 * This version implements a simplified version of the GTP protocol.
 *
 * Generally, the GTP protocol produces binary serialized files on the
 * basis of three main structures: Element (simple scalars), Arrays
 * (nD collections of numbers of the same type) and Cells (nD Collections
 * of other structures. May containt multi-type content.)
 *
 * The current implementation supports Elements, Arrays up to 2D
 * and Cells up to 1D.
 */

#ifndef ENCODER_H_INCLUDED
#define ENCODER_H_INCLUDED

#include <math.h>
#include "gtp_vartypes.h"
#include "gtp_buffer.h"
#include "gtp_blepacket.h"
#include "gtp_structures.h"

/**
  * @brief Builds BLE packets in order to be sent to the server through the mobile phone
  *
  * @param Transaction_Idx      The Idx of the WISH-SERVER transaction
  * @param DataContainer        A general object array that contains the addresses of the data
  * @param ContainerLength      The length of the DataContainer
  * @param dtTYpes              An array that describes the types of the data
  * @param dtTYpesLength        The length of the dtTYpes
  * @param bleBuffer            An empty pointer which is going to initiliased in this function and
  *                             will contain all the ble packets that are going to be sent to the server
  * @param NumberOfBLEPackets   Gets the number of ble packets that are going to be sent
  */
void Build_BLE_Packets( cGTP_UINT8 Transaction_Idx,
                        cGTP_OBJECT_ptr *DataContainer,
                        cGTP_UINT32 ContainerLength,
                        cGTP_DATA_TYPES *dtTYpes,
                        cGTP_UINT32 dtTYpesLength,
                        BLE_BUFFER *bleBuffer );

/*
 *  _____  Encoders  __________________________________________________
 *
 */

// used only when all the data that are going to be send are elements
void Encoder0(  cGTP_OBJECT_ptr *DataContainer,
                cGTP_UINT32 ContainerLength,
                cGTP_DATA_TYPES *dtTYpes,
                cGTP_UINT32 dtTYpesLength,
                BUFFER *Buffer);

// more general encoder that considers the header of each structure in order to serialize correctly the data
void Encoder(   cGTP_OBJECT_ptr* DataContainer,
                cGTP_UINT32 ContainerLength,
                BUFFER* Buffer);

/*
 *  _____  Writers  __________________________________________________
 *
 */

void WriteData(StctrObj *DataContainer, cGTP_OBJECT_ptr DestinationPtr, cGTP_BUFFER_CATEGORIES DestCategory, cGTP_BOOL InCellFlag);
void WriteHeader(StructureHeader *Header, cGTP_OBJECT_ptr DestinationPtr, cGTP_BUFFER_CATEGORIES DestCategory, cGTP_BOOL IsCellFlag);
void Write2Destination(cGTP_OBJECT_ptr Val, cGTP_DATA_TYPES dtType, cGTP_OBJECT_ptr DestinationPtr, cGTP_UINT32* WriteAtPosition, cGTP_BUFFER_CATEGORIES DestCategory);
cGTP_UINT32 CalculateContainerDataBytes0(cGTP_UINT32 ContainerLength, cGTP_DATA_TYPES *DdtTYpes, cGTP_UINT32 dtTYpesLength);
cGTP_UINT32 CalculateContainerDataBytes(cGTP_OBJECT_ptr *DataContainer, cGTP_UINT32 ContainerLength, cGTP_BOOL IsCellFlag);

#endif // ENCODER_H_INCLUDED
