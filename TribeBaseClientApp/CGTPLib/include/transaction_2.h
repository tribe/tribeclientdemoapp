/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef TRANSACTION_2_H_INCLUDED
#define TRANSACTION_2_H_INCLUDED

#include "../Transactions/transaction_id.h"
#include "../Structures/gtp_structures.h"

#define TR2_TRANSACTION_ID 2

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

extern BASE_DATA_SCHEME Transaction_2_SchemaDescription[];

/*
 *  _____  DATA SCHEMA INTERFACE  __________________________________________________
 *
 */

// There are the data, which you want to be encoded
// from here -->
extern cGTP_SINGLE TR2_Number_Of_Total_Bytes;
extern cGTP_SINGLE TR2_Number_of_Branches;
extern cGTP_SINGLE TR2_Max_branch_length;
extern cGTP_INT8   **TR2_Branch_Logic;
extern cGTP_SINGLE **TR2_Branch_Values;
extern cGTP_INT8   **TR2_Branch_Input_Vector_Index;
extern cGTP_INT8   *TR2_Class_Labels;
extern cGTP_SINGLE ***TR2_BOW;
extern cGTP_SINGLE **TR2_Slope_Factors;
// <-- to here

// this is an array that holds the shape of BOW
extern cGTP_INT16*  TR2_BOW_Shape;


//These are the gtp structures that handle the above pure C data
// form here -->
extern StctrObj TR2_Number_of_Branches_Elem;
extern StctrObj TR2_Max_branch_length_Elem;
extern StctrObj TR2_Branch_Logic_Array;
extern StctrObj TR2_Branch_Values_Array;
extern StctrObj TR2_Branch_Input_Vector_Index_Array;
extern StctrObj TR2_Class_Labels_Array;
extern StctrObj TR2_BOW_Cell;
extern StctrObj TR2_Slope_Factors_Array;
// <-- to here

// IMPORTANT! Since the length of BOW Cell is fixed and equal to 4,
// these gtp structures are going to handle each one of the 4 BOW elements.
// -->
extern StctrObj TR2_BOW_1_Array;
extern StctrObj TR2_BOW_2_Array;
extern StctrObj TR2_BOW_3_Array;
extern StctrObj TR2_BOW_4_Array;
// <--
// and this array of pointers is going to point to the above 4 structures
// and be handled by the TR2_BOW_Cell.
extern StctrObj* TR2_BOWc[];
// <--

// The following array is the container that will hold the addresses
// of the gtp structures (TR2_Number_Of_Total_Bytes_Elem, etc) in order to be fed
// to the encoder.
extern cGTP_OBJECT_ptr TR2_DataContainer[];
extern cGTP_UINT16     TR2_ContainerLength;


/*
 *  _____  FUNCTIONS  __________________________________________________
 *
 */

// Read the from the Transactions.C file the Transaction_ID_2_Update_Lexicon_From_WISH_2_SERVER() example
// in order to have an idea hot to use the following functions correctly.
void Init_Transaction_2_Data(cGTP_OBJECT_ptr InitArray[]);
void Wrap_Transaction_2_Data();
void Clear_Transaction_2_Structures();
void Clear_Transaction_2_Data();

#endif // TRANSACTION_2_H_INCLUDED
