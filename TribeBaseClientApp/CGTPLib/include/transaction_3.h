/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */


#ifndef TRANSACTION_3_H_INCLUDED
#define TRANSACTION_3_H_INCLUDED

#include <stdio.h>
#include "transaction_id.h"
#include "gtp_structures.h"
#include "cJSON.h"
#include "gtp_buffer.h"
#include "cgtpdebug.h"

#define TR3_TRANSACTION_ID 3

#define TR3_SQUAT_OUTPUT
#ifndef TR3_SQUAT_OUTPUT
    #define TR3_RUN_OUTPUT
#endif // TR3_SQUAT_OUTPUT

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

extern BASE_DATA_SCHEME Transaction_3_SchemaDescription[];

/*
 *  _____  DATA SCHEMA INTERFACE  __________________________________________________
 *
 */

// There are the data, which you want to be encoded
// from here -->
extern cGTP_SINGLE TR3_Number_Of_Total_Bytes;
extern cGTP_SINGLE TR3_Pedometer_Step_Counter;
extern cGTP_SINGLE TR3_Pedometer_Step_Position;
extern cGTP_SINGLE TR3_Pedometer_Step_Duration;
extern cGTP_SINGLE TR3_Pedometer_Step_Frequency;
extern cGTP_SINGLE TR3_Pedometer_Step_Length;
extern cGTP_SINGLE TR3_Pedometer_Average_Step_Length;
extern cGTP_SINGLE TR3_Pedometer_Cadence;
extern cGTP_SINGLE TR3_Pedometer_Pace;
extern cGTP_SINGLE TR3_Pedometer_Total_Distance;
extern cGTP_SINGLE TR3_Pedometer_Active_Distance;
extern cGTP_SINGLE TR3_Timer_Total_Duration;
extern cGTP_SINGLE TR3_Timer_Active_Duration;
extern cGTP_SINGLE TR3_Velocimeter_Speed;
extern cGTP_SINGLE TR3_Accelerometer_Acceleration_Y;
extern cGTP_SINGLE TR3_Accelerometer_Deceleration_Y;
extern cGTP_SINGLE TR3_Energymeter_Running_Energy_4_Current_Step;
extern cGTP_SINGLE TR3_Energymeter_Risk_Energy_4_Current_Step;
extern cGTP_SINGLE TR3_Energymeter_Waste_Energy_4_Current_Step;
extern cGTP_SINGLE TR3_Calorimeter_Running_Energy_2_Calories;
extern cGTP_SINGLE TR3_Calorimeter_Risk_Energy_2_Calories;
extern cGTP_SINGLE TR3_Calorimeter_Waste_Energy_2_Calories;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_STEP_FREQUENCY;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_STEP_FREQUENCY_TOTAL;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_LOCAL_EXTREMA_Z_MIN;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_LOCAL_EXTREMA_Z_MAX;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_W100;
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_W500;

#ifdef TR3_RUN_OUTPUT
extern cGTP_SINGLE TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_TOTAL;
extern cGTP_SINGLE TR3_Stats_Velocimeter_Speed_mean;
extern cGTP_SINGLE TR3_Stats_Pedometer_Step_Duration_mean;
#else
#ifdef TR3_SQUAT_OUTPUT
extern cGTP_SINGLE TR3_Distometer_Displacement_Relative;
extern cGTP_SINGLE TR3_Velocity_Up_Relative;
extern cGTP_SINGLE TR3_Velocity_Down_Relative;
#endif // TR3_SQUAT_OUTPUT
#endif // TR3_RUN_OUTPUT

extern cGTP_SINGLE TR3_Stats_Pedometer_Step_Frequency_mean;
extern cGTP_SINGLE TR3_Stats_Pedometer_Step_Length_mean;
extern cGTP_SINGLE TR3_Stats_Pedometer_Average_Step_Length_mean;
extern cGTP_SINGLE TR3_Stats_Pedometer_Cadence_mean;
extern cGTP_SINGLE TR3_Stats_Pedometer_Pace_mean;
extern cGTP_SINGLE TR3_Stats_Energymeter_Running_Energy_4_Current_Step_mean;
extern cGTP_SINGLE TR3_Stats_Energymeter_Risk_Energy_4_Current_Step_mean;
extern cGTP_SINGLE TR3_Stats_Energymeter_Waste_Energy_4_Current_Step_mean;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_GMed;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_GM;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_BF;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_VLVM;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_Add;
extern cGTP_SINGLE TR3_INTENSITY_TOTAL_RF;
extern cGTP_SINGLE TR3_INTENSITY_Right_GMed;
extern cGTP_SINGLE TR3_INTENSITY_Right_GM;
extern cGTP_SINGLE TR3_INTENSITY_Right_BF;
extern cGTP_SINGLE TR3_INTENSITY_Right_VLVM;
extern cGTP_SINGLE TR3_INTENSITY_Right_Add;
extern cGTP_SINGLE TR3_INTENSITY_Right_RF;
extern cGTP_SINGLE TR3_INTENSITY_Left_GMed;
extern cGTP_SINGLE TR3_INTENSITY_Left_GM;
extern cGTP_SINGLE TR3_INTENSITY_Left_BF;
extern cGTP_SINGLE TR3_INTENSITY_Left_VLVM;
extern cGTP_SINGLE TR3_INTENSITY_Left_Add;
extern cGTP_SINGLE TR3_INTENSITY_Left_RF;
extern cGTP_SINGLE TR3_SYMMETRY_Right_GMed;
extern cGTP_SINGLE TR3_SYMMETRY_Right_GM;
extern cGTP_SINGLE TR3_SYMMETRY_Right_BF;
extern cGTP_SINGLE TR3_SYMMETRY_Right_VLVM;
extern cGTP_SINGLE TR3_SYMMETRY_Right_Add;
extern cGTP_SINGLE TR3_SYMMETRY_Right_RF;
extern cGTP_SINGLE TR3_SYMMETRY_Left_GMed;
extern cGTP_SINGLE TR3_SYMMETRY_Left_GM;
extern cGTP_SINGLE TR3_SYMMETRY_Left_BF;
extern cGTP_SINGLE TR3_SYMMETRY_Left_VLVM;
extern cGTP_SINGLE TR3_SYMMETRY_Left_Add;
extern cGTP_SINGLE TR3_SYMMETRY_Left_RF;
extern cGTP_SINGLE TR3_ENERGY_TOTAL;
extern cGTP_SINGLE TR3_EFFICIENCY_TOTAL;
extern cGTP_SINGLE TR3_FATIGUE_BF_RIGHT;
extern cGTP_SINGLE TR3_FATIGUE_BF_LEFT;
extern cGTP_SINGLE TR3_FATIGUE_VLVM_RIGHT;
extern cGTP_SINGLE TR3_FATIGUE_VLVM_LEFT;
extern cGTP_SINGLE TR3_FATIGUE_RF_RIGHT;
extern cGTP_SINGLE TR3_FATIGUE_RF_LEFT;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_GMed_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_GM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_BF_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_VLVM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_Add_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_TOTAL_RF_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_GMed_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_GM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_BF_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_VLVM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_Add_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Right_RF_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_GMed_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_GM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_BF_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_VLVM_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_Add_MEAN;
extern cGTP_SINGLE TR3_STATS_INTENSITY_Left_RF_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_GMed_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_GM_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_BF_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_VLVM_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_Add_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Right_RF_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_GMed_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_GM_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_BF_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_VLVM_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_Add_MEAN;
extern cGTP_SINGLE TR3_STATS_SYMMETRY_Left_RF_MEAN;
extern cGTP_SINGLE TR3_STATS_ENERGY_TOTAL_MEAN;
extern cGTP_SINGLE TR3_STATS_EFFICIENCY_TOTAL_MEAN;
extern cGTP_SINGLE TR3_Energymeter_Transport_Work_4_Current_Step;
extern cGTP_SINGLE TR3_Calorimeter_Transport_Work_2_Calories;
extern cGTP_SINGLE TR3_Stats_Energymeter_Transport_Work_4_Current_Step_mean;
extern cGTP_SINGLE TR3_GPS_LATITUDE;
extern cGTP_SINGLE TR3_GPS_LONGITUDE;
extern cGTP_SINGLE TR3_GPS_ELEVATION;
extern cGTP_SINGLE TR3_GPS_ACCURACY;
extern cGTP_SINGLE TR3_GPS_TOTAL_DISTANCE;
extern cGTP_SINGLE TR3_GPS_SPEED;
// <-- to here

//These are the gtp structures that handle the above pure C data
// form here -->
extern StctrObj TR3_Number_Of_Total_Bytes_Elem;;
extern StctrObj TR3_Pedometer_Step_Counter_Elem;
extern StctrObj TR3_Pedometer_Step_Position_Elem;
extern StctrObj TR3_Pedometer_Step_Duration_Elem;
extern StctrObj TR3_Pedometer_Step_Frequency_Elem;
extern StctrObj TR3_Pedometer_Step_Length_Elem;
extern StctrObj TR3_Pedometer_Average_Step_Length_Elem;
extern StctrObj TR3_Pedometer_Cadence_Elem;
extern StctrObj TR3_Pedometer_Pace_Elem;
extern StctrObj TR3_Pedometer_Total_Distance_Elem;
extern StctrObj TR3_Pedometer_Active_Distance_Elem;
extern StctrObj TR3_Timer_Total_Duration_Elem;
extern StctrObj TR3_Timer_Active_Duration_Elem;
extern StctrObj TR3_Velocimeter_Speed_Elem;
extern StctrObj TR3_Accelerometer_Acceleration_Y_Elem;
extern StctrObj TR3_Accelerometer_Deceleration_Y_Elem;
extern StctrObj TR3_Energymeter_Running_Energy_4_Current_Step_Elem;
extern StctrObj TR3_Energymeter_Risk_Energy_4_Current_Step_Elem;
extern StctrObj TR3_Energymeter_Waste_Energy_4_Current_Step_Elem;
extern StctrObj TR3_Calorimeter_Running_Energy_2_Calories_Elem;
extern StctrObj TR3_Calorimeter_Risk_Energy_2_Calories_Elem;
extern StctrObj TR3_Calorimeter_Waste_Energy_2_Calories_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_STEP_FREQUENCY_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_STEP_FREQUENCY_TOTAL_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_LOCAL_EXTREMA_Z_MIN_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_LOCAL_EXTREMA_Z_MAX_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_W100_Elem;
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_W500_Elem;

#ifdef TR3_RUN_OUTPUT
extern StctrObj TR3_K1_ESTIMATION_PARAMS_AVERAGE_ABS_ACCEL_Z_AMP_TOTAL_Elem;
extern StctrObj TR3_Stats_Velocimeter_Speed_mean_Elem;
extern StctrObj TR3_Stats_Pedometer_Step_Duration_mean_Elem;
#else
#ifdef TR3_SQUAT_OUTPUT
extern StctrObj TR3_Distometer_Displacement_Relative_Elem;
extern StctrObj TR3_Velocity_Up_Relative_Elem;
extern StctrObj TR3_Velocity_Down_Relative_Elem;
#endif // TR3_SQUAT_OUTPUT
#endif // TR3_RUN_OUTPUT

extern StctrObj TR3_Stats_Pedometer_Step_Frequency_mean_Elem;
extern StctrObj TR3_Stats_Pedometer_Step_Length_mean_Elem;
extern StctrObj TR3_Stats_Pedometer_Average_Step_Length_mean_Elem;
extern StctrObj TR3_Stats_Pedometer_Cadence_mean_Elem;
extern StctrObj TR3_Stats_Pedometer_Pace_mean_Elem;
extern StctrObj TR3_Stats_Energymeter_Running_Energy_4_Current_Step_mean_Elem;
extern StctrObj TR3_Stats_Energymeter_Risk_Energy_4_Current_Step_mean_Elem;
extern StctrObj TR3_Stats_Energymeter_Waste_Energy_4_Current_Step_mean_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_GMed_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_GM_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_BF_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_VLVM_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_Add_Elem;
extern StctrObj TR3_INTENSITY_TOTAL_RF_Elem;
extern StctrObj TR3_INTENSITY_Right_GMed_Elem;
extern StctrObj TR3_INTENSITY_Right_GM_Elem;
extern StctrObj TR3_INTENSITY_Right_BF_Elem;
extern StctrObj TR3_INTENSITY_Right_VLVM_Elem;
extern StctrObj TR3_INTENSITY_Right_Add_Elem;
extern StctrObj TR3_INTENSITY_Right_RF_Elem;
extern StctrObj TR3_INTENSITY_Left_GMed_Elem;
extern StctrObj TR3_INTENSITY_Left_GM_Elem;
extern StctrObj TR3_INTENSITY_Left_BF_Elem;
extern StctrObj TR3_INTENSITY_Left_VLVM_Elem;
extern StctrObj TR3_INTENSITY_Left_Add_Elem;
extern StctrObj TR3_INTENSITY_Left_RF_Elem;
extern StctrObj TR3_SYMMETRY_Right_GMed_Elem;
extern StctrObj TR3_SYMMETRY_Right_GM_Elem;
extern StctrObj TR3_SYMMETRY_Right_BF_Elem;
extern StctrObj TR3_SYMMETRY_Right_VLVM_Elem;
extern StctrObj TR3_SYMMETRY_Right_Add_Elem;
extern StctrObj TR3_SYMMETRY_Right_RF_Elem;
extern StctrObj TR3_SYMMETRY_Left_GMed_Elem;
extern StctrObj TR3_SYMMETRY_Left_GM_Elem;
extern StctrObj TR3_SYMMETRY_Left_BF_Elem;
extern StctrObj TR3_SYMMETRY_Left_VLVM_Elem;
extern StctrObj TR3_SYMMETRY_Left_Add_Elem;
extern StctrObj TR3_SYMMETRY_Left_RF_Elem;
extern StctrObj TR3_ENERGY_TOTAL_Elem;
extern StctrObj TR3_EFFICIENCY_TOTAL_Elem;
extern StctrObj TR3_FATIGUE_BF_RIGHT_Elem;
extern StctrObj TR3_FATIGUE_BF_LEFT_Elem;
extern StctrObj TR3_FATIGUE_VLVM_RIGHT_Elem;
extern StctrObj TR3_FATIGUE_VLVM_LEFT_Elem;
extern StctrObj TR3_FATIGUE_RF_RIGHT_Elem;
extern StctrObj TR3_FATIGUE_RF_LEFT_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_GMed_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_GM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_BF_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_VLVM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_Add_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_TOTAL_RF_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_GMed_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_GM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_BF_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_VLVM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_Add_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Right_RF_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_GMed_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_GM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_BF_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_VLVM_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_Add_MEAN_Elem;
extern StctrObj TR3_STATS_INTENSITY_Left_RF_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_GMed_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_GM_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_BF_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_VLVM_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_Add_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Right_RF_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_GMed_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_GM_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_BF_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_VLVM_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_Add_MEAN_Elem;
extern StctrObj TR3_STATS_SYMMETRY_Left_RF_MEAN_Elem;
extern StctrObj TR3_STATS_ENERGY_TOTAL_MEAN_Elem;
extern StctrObj TR3_STATS_EFFICIENCY_TOTAL_MEAN_Elem;
extern StctrObj TR3_Energymeter_Transport_Work_4_Current_Step_Elem;
extern StctrObj TR3_Calorimeter_Transport_Work_2_Calories_Elem;
extern StctrObj TR3_Stats_Energymeter_Transport_Work_4_Current_Step_mean_Elem;
extern StctrObj TR3_GPS_LATITUDE_Elem;
extern StctrObj TR3_GPS_LONGITUDE_Elem;
extern StctrObj TR3_GPS_ELEVATION_Elem;
extern StctrObj TR3_GPS_ACCURACY_Elem;
extern StctrObj TR3_GPS_TOTAL_DISTANCE_Elem;
extern StctrObj TR3_GPS_SPEED_Elem;
// <-- to here


// The following array is the container that will hold the addresses
// of the gtp structures (TR2_Number_Of_Total_Bytes_Elem, etc) in order to be fed
// to the encoder.
extern cGTP_OBJECT_ptr TR3_DataContainer[];
extern cGTP_UINT16     TR3_ContainerLength;

/*
 *  _____  FUNCTIONS  __________________________________________________
 *
 */

// Read the from the Transactions.C file the Transaction_ID_2_Update_Lexicon_From_WISH_2_SERVER() example
// in order to have an idea hot to use the following functions correctly.
void Init_Transaction_3_Structures();
void Wrap_Transaction_3_Structures();
void Clear_Transaction_3_Structures();
void Clear_Transaction_3_Data();
char* GetTrans3Data(char* DataSchema);

#endif // TRANSACTION_3_H_INCLUDED
