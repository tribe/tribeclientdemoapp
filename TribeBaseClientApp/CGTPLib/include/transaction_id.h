/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef TRANSACTION_ID_H_INCLUDED
#define TRANSACTION_ID_H_INCLUDED

#include "gtp_vartypes.h"

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

 typedef struct BASE_DATA_SCHEME{
     cGTP_STRUCTURE_TYPES StructureType;
     cGTP_DATA_TYPES DataType;
 } BASE_DATA_SCHEME;


#endif // TRANSACTION_ID_H_INCLUDED
