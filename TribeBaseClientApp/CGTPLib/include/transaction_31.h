/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef TRANSACTION_31_H_INCLUDED
#define TRANSACTION_31_H_INCLUDED

#include "../Transactions/transaction_id.h"
#include "../Structures/gtp_structures.h"

#define TR31_TRANSACTION_ID 31

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

extern BASE_DATA_SCHEME Transaction_31_SchemaDescription[];

/*
 *  _____  DATA SCHEMA INTERFACE  __________________________________________________
 *
 */


extern cGTP_UINT8 **TR31_BOW;
extern cGTP_UINT8*  TR31_BOW_Shape;

extern StctrObj TR31_BOW_Cell;
extern StctrObj TR31_BOW_Array;
extern StctrObj* TR31_BOWc[];

extern cGTP_OBJECT_ptr TR31_DataContainer[];
extern cGTP_UINT16     TR31_ContainerLength;


/*
 *  _____  FUNCTIONS  __________________________________________________
 *
 */

// Read the from the Transactions.C file the Transaction_ID_2_Update_Lexicon_From_WISH_2_SERVER() example
// in order to have an idea hot to use the following functions correctly.
void Init_Transaction_31_Data(cGTP_OBJECT_ptr InitArray[]);
void Wrap_Transaction_31_Data();
void Clear_Transaction_31_Structures();
void Clear_Transaction_31_Data();


#endif // TRANSACTION_31_H_INCLUDED
