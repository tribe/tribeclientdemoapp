/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

/*
 * This version implements a simplified version of the GTP protocol.
 *
 * Generally, the GTP protocol produces binary serialized files on the
 * basis of three main structures: Element (simple scalars), Arrays
 * (nD collections of numbers of the same type) and Cells (nD Collections
 * of other structures. May containt multi-type content.)
 *
 * The current implementation supports Elements, Arrays up to 2D
 * and Cells up to 1D.
 */

#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

#include <stdlib.h>
#include "gtp_vartypes.h"


/*
 *  _____  Description of the general Structure  __________________________________________________
 *
 */

typedef struct StructureHeader
{
    HEADER_STR_TYPE  StructureType;
    HEADER_DIM_TYPE  Dimensions;
    HEADER_SHP_TYPE  Shape[2];
    HEADER_DAT_TYPE  DataType;
} StructureHeader;

#define Def_StructureHeader(Name) StructureHeader Name = {.Shape = NULL};

#define STRUCTURE_GENERATOR(Type)   typedef struct{              \
                                        StructureHeader*  Header;\
                                        Type Data;               \
                                        cGTP_BOOL IsDynamic;          \
                                    }

// General structure object
// you could define a type-specific object as:
// STRUCTURE(cGTP_SINGLE*) StctrObj;

/**
 * @brief A structure object could be an element, array of cell
 *
 */
STRUCTURE_GENERATOR(cGTP_OBJECT_ptr**) StctrObj;

// for definition and initialisation
#define Def_Structure(StructureType, Name) StructureType Name = {.Header = NULL, .Data = NULL};


/*
 *  _____  Tools for accessing structure's data  __________________________________________________
 *
 */

#define GetDataStat(Type, Name, NoCols, i, j) *((Type*) Name.Data + i * NoCols + j)
#define GetDataDyn2(Type, Name, i, j)          ((Type**)Name.Data)[i][j]
#define GetDataDyn1(Type, Name, i)             ((Type*) Name.Data)[i]
#define GetDataDyn0(Type, Name)                *(Type*) Name.Data

#define GetCellDataStat(Type, Name, Idx, NoCols, i, j) *((Type*)Name.Data[Idx] + i * NoCols + j)
#define GetCellDataStatPtr(Type, Name, Idx, NoCols, i, j) ((Type*)Name.Data[Idx] + i * NoCols + j)
//#define GetCellDataDyn1(Type, Name, i) *((Type *)Name.Data[Idx] + i * NoCols + j)
//#define GetCellDataDyn0(Type, Name) *(Type *)Name.Data

#define GetData0D(Type, StrName)       ( StrName.IsDynamic ? GetDataDyn0(Type, StrName) : GetDataStat(Type, StrName, 0, 0, 0) )
#define GetData1D(Type, StrName, j)    ( StrName.IsDynamic ? GetDataDyn1(Type, StrName, j) : GetDataStat(Type, StrName, 0, 0, j) )
#define GetData2D(Type, StrName, i, j) ( StrName.IsDynamic ? GetDataDyn2(Type, StrName, i, j) : GetDataStat(Type, StrName, (cGTP_UINT32)StrName.Header->Shape[1], i, j) )

#define GetCellData0D(Type, StrName)      (StrName.IsDynamic ? GetCellDataStat(Type, StrName, 0, 0, 0, 0) : GetCellDataStat(Type, StrName, 0, 0, 0, 0))
#define GetCellData1D(Type, StrName, Idx) (StrName.IsDynamic ? GetCellDataStat(Type, StrName, Idx, 0, 0, 0) : GetCellDataStat(Type, StrName, Idx, 0, 0, 0))

#define GetCellDataPtr0D(Type, StrName)      (StrName.IsDynamic ? GetCellDataStatPtr(Type, StrName, 0, 0, 0, 0) : GetCellDataStatPtr(Type, StrName, 0, 0, 0, 0))
#define GetCellDataPtr1D(Type, StrName, Idx) (StrName.IsDynamic ? GetCellDataStatPtr(Type, StrName, Idx, 0, 0, 0) : GetCellDataStatPtr(Type, StrName, Idx, 0, 0, 0))

/**
 * @brief Returns the data value from an element object.
 *
 * @param Obj the element structure for which we want to get the data
 * @return cGTP_SINGLE the output is a cGTP_SINGLE number. However, the element may be of different data type
 */
cGTP_SINGLE GetDataWH_0D(StctrObj* Obj);

/**
 * @brief returns the reference/pointer of the data of an element structure
 *
 * @param Obj the element structure
 * @param cVar the reference to the data of the element
 */
void GetDataWH_0D_ref(StctrObj* Obj, cGTP_OBJECT_ptr* cVar);

/**
 * @brief Returns the j-th data value - data[j] - from a 1D array object
 *
 * @param Obj the array structure
 * @param j the desired idx
 * @return cGTP_SINGLE the output is always a cGTP_SINGLE number
 */
cGTP_SINGLE GetDataWH_1D(StctrObj* Obj, cGTP_UINT32 j);

/**
 * @brief Returns the reference/pointer of the jth data value of an 1D array structure
 *
 * @param Obj the array structure
 * @param j the desired idx
 * @param cVar the output reference
 */
void GetDataWH_1D_ref(StctrObj* Obj, cGTP_UINT32 j, cGTP_OBJECT_ptr* cVar);

/**
 * @brief Returns the (i,j)th data value - data[i][j] - from a 2D array object
 *
 * @param Obj the array structure
 * @param i the desired row idx
 * @param j the desired column idx
 * @return cGTP_SINGLE the output is always a cGTP_SINGLE number
 */
cGTP_SINGLE GetDataWH_2D(StctrObj* Obj, cGTP_UINT32 i, cGTP_UINT32 j);

/**
 * @brief Returns the reference/pointer of the (i,j)the data value of an 2D array structure
 *
 * @param Obj the array structure
 * @param i the desired row idx
 * @param j the desired column idx
 * @param cVar the output reference
 */
void GetDataWH_2D_ref(StctrObj* Obj, cGTP_UINT32 i, cGTP_UINT32 j, cGTP_OBJECT_ptr* cVar);


/*
 *  _____  Tools for initialising structured objects  __________________________________________________
 *
 */

/**
 * @brief Initialises the header of a structure
 *
 * @param Header The header pointer
 * @param StrType the type of the structure (element, array, cell)
 * @param Dims the number of dimensions for the case of an array or cell
 * @param Shape the shape of the structure. (e.g. for a 2D array: shape[0] = 2 and shape[1] = 3.
 *              the first number represents the rows, while the second the columns)
 * @param Datatype The type of the data (e.g. cGTP_dt_Single (cGTP_SINGLE), cGTP_dt_Uint32 (cGTP_UINT32), etc)
 */
void StructureHeader_Init(  StructureHeader** Header,
                            HEADER_STR_TYPE  StrType,
                            HEADER_DIM_TYPE  Dims,
                            HEADER_SHP_TYPE  Shape[],
                            HEADER_DAT_TYPE  Datatype   );

/**
 * @brief Clears the allocated memory from a structure
 *
 * @param Obj the address of the structure
 */
void ClearStctr(cGTP_OBJECT_ptr Obj);


void Init_Element(  cGTP_OBJECT_ptr Elem,
                    cGTP_BOOL IsDynamic,
                    HEADER_DAT_TYPE dtType,
                    cGTP_BOOL InitHeaderFlag );

void Init_Array(    cGTP_OBJECT_ptr Array,
                    cGTP_BOOL IsDynamic,
                    cGTP_UINT32 NoRows,
                    cGTP_UINT32 NoCols,
                    HEADER_DAT_TYPE dtType  );

void Init_Cell( cGTP_OBJECT_ptr Cell,
                cGTP_BOOL IsDynamic,
                cGTP_UINT32 NoRows,
                cGTP_UINT32 NoCols,
                HEADER_DAT_TYPE dtType  );

/**
 * @brief Wraps the data and connects them with an element structure
 *
 * @param Elem the address of the element structure
 * @param Val the address of the value that should be wrapped
 * @param dtType the type of the value
 * @param InitHeaderFlag a flag that is used to determin if the element would have or not a header
 */
void Wrap_Element(  cGTP_OBJECT_ptr Elem,
                    cGTP_OBJECT_ptr Val,
                    HEADER_DAT_TYPE dtType,
                    cGTP_BOOL InitHeaderFlag );

/**
 * @brief Wraps the data and connects them with an array structure. Currently only arrays up to 2D are supported.
 *
 * @param Array the address of the array structure
 * @param SrcArray the address of the source array data
 * @param IsDynamic a flag that determines whether the source array is dynamically allocated or not
 * @param NoRows Number of rows
 * @param NoCols Number of columns
 * @param dtType Type of the source array
 */
void Wrap_Array(    cGTP_OBJECT_ptr Array,
                    cGTP_OBJECT_ptr SrcArray,
                    cGTP_BOOL IsDynamic,
                    cGTP_UINT32 NoRows,
                    cGTP_UINT32 NoCols,
                    HEADER_DAT_TYPE dtType  );

/**
 * @brief Wraps the data and connects them with a cell structure. Currently only cells up to 1D are supported.
 *
 * @param Cell the address of the cell structure
 * @param SrcCell the address of the source cell data
 * @param IsDynamic a flag that determines whether the source cell is dynamically allocated or not
 * @param NoRows Number of rows
 * @param NoCols Number of columns
 * @param dtType Type of the source cell (the cell are multi-type objects thus, this input should be NULL)
 */
void Wrap_Cell( cGTP_OBJECT_ptr Cell,
                cGTP_OBJECT_ptr SrcCell,
                cGTP_BOOL IsDynamic,
                cGTP_UINT32 NoRows,
                cGTP_UINT32 NoCols,
                HEADER_DAT_TYPE dtType  );

#endif // STRUCTURES_H_INCLUDED
