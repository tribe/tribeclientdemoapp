/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef BLEPACKET_H_INCLUDED
#define BLEPACKET_H_INCLUDED

#include "gtp_vartypes.h"
#include "gtp_buffer.h"

/*
 *  _____  BLE packet header  __________________________________________________
 *
 */

typedef struct BLE_Header
{
    BLE_HEADER_TRID_TYPE  Transaction_ID;
    BLE_HEADER_PACn_TYPE  Packet_No;
    BLE_HEADER_ONOP_TYPE  Overall_Number_of_Packets;
    BLE_HEADER_PLIB_TYPE  Packet_Length_in_Bytes;
} BLE_Header;

void Init_BLE_Header( BLE_Header* inputObj,
                      BLE_HEADER_TRID_TYPE  Transaction_ID,
                      BLE_HEADER_PACn_TYPE  Packet_No,
                      BLE_HEADER_ONOP_TYPE  Overall_Number_of_Packets,
                      BLE_HEADER_PLIB_TYPE  Packet_Length_in_Bytes );

/*
 *  _____  Specific Buffer : ble Buffer  __________________________________________________
 *
 */

typedef struct BLE_BUFFER
{
    BUFFER SerialBuffer;
    BUFFER_TYPE **StartOfBlePacket_ptr;
    cGTP_UINT32 *PacketLength;
    cGTP_UINT32 NumberOfPackets;
    //cGTP_UINT8  Mode;
} BLE_BUFFER;

#define DefBLE_BUFFER(Name) BLE_BUFFER Name = {.StartOfBlePacket_ptr = NULL, .PacketLength = NULL};

cGTP_INT8 Init_BLE_BUFFER  (BLE_BUFFER* InputObjref, cGTP_UINT32 BufferLength, cGTP_UINT32 NumberOfPackets);
cGTP_INT8 Clear_BLE_BUFFER (BLE_BUFFER* InputObjref);
cGTP_INT8 Write_BLE_Header_2_BLE_BUFFER(BLE_BUFFER* blebufref, BLE_Header* Headeref);
cGTP_INT8 Read_BLE_Header_From_BLE_BUFFER(BLE_BUFFER* blebufref, BLE_Header* Headeref);
cGTP_INT8 Read_BLE_Header_From_BUFFER(BUFFER* bufref, BLE_Header* Headeref);

/*
 *  _____  BLE packet design  __________________________________________________
 *
 */

typedef struct BLE_Packet
{
    BLE_Header Header;
    BLE_BUFFER BleBuffer;
} BLE_Packet;



#endif // BLEBUFFER_H_INCLUDED
