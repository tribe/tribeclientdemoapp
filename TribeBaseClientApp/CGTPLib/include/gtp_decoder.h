/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef DECODER_H_INCLUDED
#define DECODER_H_INCLUDED

#include "gtp_vartypes.h"
#include "gtp_structures.h"
#include "gtp_buffer.h"
#include "gtp_blepacket.h"
#include "transaction_id.h"
#include "cgtpdebug.h"

/*
 *  _____  Global Variables  __________________________________________________
 *
 */
extern BUFFER Decoder_Trans_Buffer;
extern cGTP_UINT8 Decoder_NumOfReceivedPackets;
extern cGTP_UINT8 Decoder_NumOfTotalPacketsThatShouldBeReceived;
extern cGTP_UINT8 Decoder_PreviousPacketID;

/*
 *  _____  Functions  __________________________________________________
 *
 */
cGTP_INT8 ReadBleHeaderFromBytesArray(cGTP_UINT8* Bytes, cGTP_UINT32 BytesLength, BLE_Header* bleHeaderef);
cGTP_INT8 ReadNumberOfTransactionTotalBytes(cGTP_UINT8* Bytes, cGTP_UINT32 BytesLength, cGTP_SINGLE* NumOfTotalBytes);
cGTP_INT8 BuildTransBufferFromIncommingBlePackets(cGTP_UINT8* Bytes, cGTP_UINT32 BytesLength, BUFFER* DestTransBuffer);
cGTP_INT8 AddPacketDataToTransBuffer();

void Decoder(cGTP_OBJECT_ptr* DataContainer,
             cGTP_UINT32 ContainerLength,
             BASE_DATA_SCHEME* SchemaDescription);

void ReadData( StctrObj *DataContainer,
               BASE_DATA_SCHEME *SchemaDescription,
               cGTP_OBJECT_ptr SourcePtr,
               cGTP_BUFFER_CATEGORIES SourceCategory,
               cGTP_BOOL InCellFlag );

void ReadHeader( StructureHeader *Header,
                 cGTP_STRUCTURE_TYPES ST_TYPE,
                 cGTP_OBJECT_ptr SourcePtr,
                 cGTP_BUFFER_CATEGORIES SourceCategory,
                 cGTP_BOOL IsCellFlag );

void ReadFromSource(   cGTP_OBJECT_ptr Val,
                       cGTP_DATA_TYPES dtType,
                       cGTP_OBJECT_ptr SourcePtr,
                       cGTP_UINT32* ReadFromPosition,
                       cGTP_BUFFER_CATEGORIES SourceCategory     );

cGTP_INT8 CheckReceivedPackets();
cGTP_INT8 CheckTransBuffer();

#endif // DECODER_H_INCLUDED
