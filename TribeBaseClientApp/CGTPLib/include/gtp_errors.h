#ifndef ERRORS_H_INCLUDED
#define ERRORS_H_INCLUDED

#define ERROR_NULL_POINTER               -10
#define ERROR_INIT_BUFFER                -11
#define ERROR_INIT_BUFFER_FROM_MEMORY    -12
#define ERROR_READING_FROM_BUFFER        -13
#define ERROR_CLEAR_MEMORY               -14
#define ERROR_BSEEK_BUFFER               -15
#define ERROR_BWRITE_BUFFER              -16
#define ERROR_BREAD_BUFFER               -17
#define ERROR_CONDITION_NOT_TRUE         -18
#define ERROR_DECODER_BLE_PACKET_IS_LOST -19
#define ERROR_DECODER_TRANSBUFFER_NOT_INIT -20

#endif // ERRORS_H_INCLUDED
