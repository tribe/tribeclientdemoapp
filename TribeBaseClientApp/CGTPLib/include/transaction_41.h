/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

/*
 *  The transaction_41 executed in the MOBILE phone
 *  and is responsible for decoding WISH gtp data
 *  that are being sent through the transaction_31 (run info data regarding which
 *  runs are saved in the WISH flash memory).
 */

#ifndef transaction_41_H_INCLUDED
#define transaction_41_H_INCLUDED

#include <stdio.h>
#include "transaction_id.h"
#include "gtp_structures.h"
#include "cJSON.h"
#include "gtp_buffer.h"

#define TR41_TRANSACTION_ID 41

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

extern BASE_DATA_SCHEME transaction_41_SchemaDescription[];

/*
 *  _____  DATA SCHEMA INTERFACE  __________________________________________________
 *
 */

extern cGTP_SINGLE TR41_Number_Of_Total_Bytes;
extern cGTP_UINT8 **TR41_BOW2D;
extern cGTP_UINT8 *TR41_BOW1D;
extern cGTP_UINT8  TR41_BOW_Shape[];

extern StctrObj TR41_Number_Of_Total_Bytes_Elem;
extern StctrObj TR41_BOW_Cell;
extern StctrObj TR41_BOW_Array;
extern StctrObj* TR41_BOWc[];

extern cGTP_OBJECT_ptr TR41_DataContainer[];
extern cGTP_UINT16     TR41_ContainerLength;


/*
 *  _____  FUNCTIONS  __________________________________________________
 *
 */

// Read the from the Transactions.C file the Transaction_ID_2_Update_Lexicon_From_WISH_2_SERVER() example
// in order to have an idea hot to use the following functions correctly.
void Init_transaction_41_Structures();
void Wrap_transaction_41_Structures();
void Clear_transaction_41_Structures();
void Clear_transaction_41_Data();
char* GetJsonStringFromTrans41Data();
void CopyTrans41Data(cGTP_UINT8** destShape, cGTP_UINT8*** destArray2D);
char* DecodeTrans41Data2Runinfo();


#endif // transaction_41_H_INCLUDED
