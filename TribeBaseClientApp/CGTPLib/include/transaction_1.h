/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */


#ifndef TRANSACTION_1_H_INCLUDED
#define TRANSACTION_1_H_INCLUDED

#include "../Transactions/transaction_id.h"
#include "../Structures/gtp_structures.h"

#define TR1_TRANSACTION_ID 1

/*
 *  _____  DATA SCHEMA  __________________________________________________
 *
 */

extern BASE_DATA_SCHEME Transaction_1_SchemaDescription[];

/*
 *  _____  DATA SCHEMA INTERFACE  __________________________________________________
 *
 */

// There are the data, which you want to be initialized by the decoder
// from here -->
extern cGTP_SINGLE TR1_Number_Of_Total_Bytes;
extern cGTP_SINGLE TR1_Number_of_Branches;
extern cGTP_SINGLE TR1_Max_branch_length;
extern cGTP_INT8   **TR1_Branch_Logic;
extern cGTP_SINGLE **TR1_Branch_Values;
extern cGTP_INT8   **TR1_Branch_Input_Vector_Index;
extern cGTP_INT8   *TR1_Class_Labels;
extern cGTP_SINGLE ***TR1_BOW;
extern cGTP_SINGLE **TR1_Slope_Factors;
// <-- to here

extern cGTP_INT32 TR1_Slope_Factors_Rows;
extern cGTP_INT32 TR1_Slope_Factors_Cols;

// this is an array that holds the shape of BOW
extern cGTP_INT16  TR1_BOW_Shape[];


//These are the gtp structures that handle the above pure C data
// form here -->
extern StctrObj TR1_Number_Of_Total_Bytes_Elem;
extern StctrObj TR1_Number_of_Branches_Elem;
extern StctrObj TR1_Max_branch_length_Elem;
extern StctrObj TR1_Branch_Logic_Array;
extern StctrObj TR1_Branch_Values_Array;
extern StctrObj TR1_Branch_Input_Vector_Index_Array;
extern StctrObj TR1_Class_Labels_Array;
extern StctrObj TR1_BOW_Cell;
extern StctrObj TR1_Slope_Factors_Array;
// <-- to here

// IMPORTANT! Since the length of BOW Cell is fixed and equal to 4,
// these gtp structures are going to handle each one of the 4 BOW elements.
// -->
extern StctrObj TR1_BOW_1_Array;
extern StctrObj TR1_BOW_2_Array;
extern StctrObj TR1_BOW_3_Array;
extern StctrObj TR1_BOW_4_Array;
// <--
// and this array of pointers is going to point to the above 4 structures
// and be handled by the TR1_BOW_Cell.
extern StctrObj* TR1_BOWc[];
// <--

// The following array is the container that will hold the addresses
// of the gtp structures (TR1_Number_Of_Total_Bytes_Elem, etc) in order to be fed
// to the decoder.
extern cGTP_OBJECT_ptr TR1_DataContainer[];
extern cGTP_UINT16     TR1_ContainerLength;

// IMPORTANT!!! the decoder allocates memory for the above pure C data (TR1_Number_of_Branches, etc)

/*
 *  _____  FUNCTIONS  __________________________________________________
 *
 */

// Read the from the Transactions.C file the Transaction_ID_1_Update_Lexicon_From_SERVER_2_WISH() example
// in order to have an idea hot to use the following functions correctly.
void Init_Transaction_1_Structures();
void Wrap_Transaction_1_Structures();
void Clear_Transaction_1_Structures();
void Clear_Transaction_1_Data();

#endif // TRANSACTION_1_H_INCLUDED
