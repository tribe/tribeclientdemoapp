/*
 *  =============================================
 *
 *  Copyright Tribe Wearables, 2017
 *  All Rights Reserved
 *  UNPUBLISHED, LICENSED SOFTWARE.
 *
 *  CONFIDENTIAL AND PROPRIETARY INFORMATION
 *  WHICH IS THE PROPERTY OF Tribe Wearables.
 *
 *  =============================================
 *
 *  AUTHOR: Evangelos G. Karakasis
 *
 *  =============================================
 */

#ifndef PROTOCOLINFO_H_INCLUDED
#define PROTOCOLINFO_H_INCLUDED

#include "../VarTypes/gtp_vartypes.h"

#define PR_Dimensions_ -1
#define PR_NULL_ -2

/*
 *  _____  HEADER_STRUCTURE  __________________________________________________
 *
 */

typedef struct HEADER_FIELD_DES
{
    cGTP_DATA_TYPES Type;
    cGTP_INT8 Length;
} HEADER_FIELD_DES;

#define EMPTY_HEADER_FIELD {cGTP_dt_NULL,  PR_NULL_}

typedef struct HEADER_STRUCTURE
{
    HEADER_FIELD_DES StructureType;
    HEADER_FIELD_DES Dimensions;
    HEADER_FIELD_DES Shape;
    HEADER_FIELD_DES StructureDataType;
} HEADER_STRUCTURE;

#define EMPTY_HEADER_STRUCTURE {EMPTY_HEADER_FIELD, EMPTY_HEADER_FIELD, EMPTY_HEADER_FIELD, EMPTY_HEADER_FIELD}

#define INIT_HEADER_STRUCTURE   {HEADER_STR_TYPE_TYPE,  1},             /*StructureType*/     \
                                {HEADER_DIM_TYPE_TYPE,  1},             /*Dimensions*/        \
                                {HEADER_SHP_TYPE_TYPE, PR_Dimensions_}, /*Shape*/             \
                                {HEADER_DAT_TYPE_TYPE,  1}              /*StructureDataType*/

/*
 *  _____  SUPPORTED_STRUCTURES_TYPES_DEFINITION  __________________________________________________
 *
 */

typedef struct ITEM_DESCRIPTION
{
    HEADER_STRUCTURE ElementHeader;
    HEADER_STRUCTURE ArrayHeader;
    HEADER_STRUCTURE CellHeader;
} ITEM_DESCRIPTION;

#define EMPTY_ITEM_DESCRIPTION {EMPTY_HEADER_STRUCTURE, EMPTY_HEADER_STRUCTURE, EMPTY_HEADER_STRUCTURE}

#define INIT_ElementHeader  {HEADER_STR_TYPE_TYPE,  1},     /*StructureType*/     \
                             EMPTY_HEADER_FIELD,            /*Dimensions*/        \
                             EMPTY_HEADER_FIELD,            /*Shape*/             \
                            {HEADER_DAT_TYPE_TYPE,  1}      /*StructureDataType*/

#define INIT_ArrayHeader    INIT_HEADER_STRUCTURE

#define INIT_CellHeader     {HEADER_STR_TYPE_TYPE,  1},             /*StructureType*/     \
                            {HEADER_DIM_TYPE_TYPE,  1},             /*Dimensions*/        \
                            {HEADER_SHP_TYPE_TYPE, PR_Dimensions_}, /*Shape*/             \
                             EMPTY_HEADER_FIELD                     /*StructureDataType*/

typedef struct BASE_STRUCTURE_DES
{
    HEADER_STRUCTURE Header;
    ITEM_DESCRIPTION ItemDescription;
} BASE_STRUCTURE_DES;

typedef struct SUPPORTED_STRUCTURES_TYPES_DEFINITION
{
    BASE_STRUCTURE_DES Element;
    BASE_STRUCTURE_DES Array;
    BASE_STRUCTURE_DES Cell;
} SUPPORTED_STRUCTURES_TYPES_DEFINITION;

#define INIT_ARRAY_n_CELL_HEADER_STRUCTURE  {HEADER_STR_TYPE_TYPE,  1},             /*StructureType*/     \
                                            {HEADER_DIM_TYPE_TYPE,  1},             /*Dimensions*/        \
                                            {HEADER_SHP_TYPE_TYPE, PR_Dimensions_}, /*Shape*/             \
                                             EMPTY_HEADER_FIELD                     /*StructureDataType*/


/*
 *  _____  PROTOCOL_INFO  __________________________________________________
 *
 */

typedef struct PROTOCOL_INFO
{
    HEADER_STRUCTURE HeaderStructure;
    SUPPORTED_STRUCTURES_TYPES_DEFINITION SupportedStructuresTypesDefinition;
} PROTOCOL_INFO;

extern PROTOCOL_INFO gtpinfo;

#endif // PROTOCOLINFO_H_INCLUDED
