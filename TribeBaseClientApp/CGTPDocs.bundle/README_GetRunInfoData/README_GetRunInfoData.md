# CGTP Documentation

[TOC]

## Installation

To install the CGTP library you have to import:

1. the static library _LibCGTP.a_ and
2. the header files that are included in the _CGTP_ folder 

into your project as well as to put the json file entitled *DataSchema.json* in the same folder with the main file. 



## Code Examples

### Example 1: Get Run Info Data

The particular example is related with the way the CGTP library should be used in order to get a set of useful information regarding the runs that are saved in the TWD. More specifically each run, which is saved in the TWD, is characterized by the following attributes:

* **Slot**: It characterizes the TWD memory position that is used to save a run and is represented by an integer value (e.g. slot 0: 1st run, slot 1: 2nd run, etc).
* **RunID**: A unique identifier per run. For each new run, the `RunID` is increased by one.
* **RunDataLength**: It describes the number of sets of metrics that are calculated every N-steps.
* **ActiveDuration**: The actual duration of a run (e.g. the time durations for which the runner has stopped his\her activity have been removed).
* **ActiveDuration_unit**: The unit which is used to measure `ActiveDuration`. Currently the used unit is minutes ( `min`).
* **ActiveDistance**: The actual distance traveled during run (e.g. distance traveled in an extremely slow pace has been removed).
* **ActiveDistance_unit**: The unit which is used to measure `ActiveDistance`. Currently the used unit is meters ( `m`).
* **Calories**: The total number of calories burned during the run.
* **Calories_unit**: The unit which is used to measure `Calories`. Currently the used unit is kilo-calories( `Kcal` or `Cal`).
* **AverageSpeed**: The average speed of the whole run.
* **AverageSpeed_unit**: The unit which is used to measure `AverageSpeed`. Currently the used unit is kilometers per hour ( `Km/h`).
* **Status**: This value describes whether a run is open (the runner is still active) or closed (the run has finished). For `Status = 1` the run is open and if `Status = 2` the run is closed.



After having imported the CGTP static library and the corresponding header files into your project, you can access the library by including the *gtp.h* file:

```c
#include "CGTP/gtp.h"
```



#### Simulate the Incoming BLE packets

The following function entitled  `SimulateBlePackets(...)` is used to simulate the packets receival process, through the ble communication, between the TWD and the mobile device.

```C
#include <stdint.h>

// simulated ble packets
uint8_t IncomingPackets0[1][124] =
{
    {31, 1, 1, 119, 0, 0, 0, 238, 66, 103, 1, 1, 0, 102, 2, 4, 0, 26, 0, 12, 0, 30, 0, 0, 0, 5, 0, 0, 0, 10, 86, 206, 62, 171, 223, 85, 66, 148, 247, 162, 64, 224, 188, 254, 64, 2, 1, 38, 0, 0, 0, 3, 0, 0, 0, 58, 81, 127, 62, 12, 223, 3, 66, 92, 205, 63, 64, 176, 222, 253, 64, 2, 2, 39, 0, 0, 0, 1, 0, 0, 0, 87, 85, 213, 61, 245, 200, 39, 65, 82, 32, 70, 63, 205, 73, 193, 64, 2, 3, 41, 0, 0, 0, 10, 0, 0, 0, 14, 2, 131, 63, 202, 136, 8, 67, 162, 67, 27, 65, 62, 16, 0, 65, 2}
};

// it is used for the simulating the packet receival process
uint8_t* SimulateBlePackets(int packetId){
    if (packetId >=0 && packetId <= 0)
        return &IncomingPackets0[packetId][0];
    return 0;
}
```



#### Check BLE Packets and Build the Buffer

The following function returns 1 when all the BLE packets have been received and the buffer has been created successfully.

```c
int AddPacketToBufferAndCheck(uint8_t* packet){
    // pass BLE packet bytes to the buffer
    if ( AddPacketDataToTransBuffer(packet) < 0 ){
        printf("Error at packet %d.\n", 1);
        return -1;
    }

    // check if ALL the ble packets received successfully.
    if ( CheckReceivedPackets() < 0 ){
         printf("Some packets may have been lost or did not received yet.\n");
         return -1;
    }
    else{
        // call the following function to check the Buffer.
        if ( CheckTransBuffer() < 0){
            printf("The Buffer has NOT been created successfully.\n");
            return -1;
        }else{
            printf("The Buffer has been created successfully.\n");
            return 1;
        }
    }

    return 0;
}
```

Note that the functions:

- `AddPacketDataToTransBuffer(packet)`
- `CheckReceivedPackets()`
- `CheckTransBuffer()`

are internal functions of the CGTP library. In fact, the `AddPacketToBufferAndCheck(packet)` organizes the aforementioned internal functions in a useful way that can be used repeatedly. When the above code "perceives" that all the needed BLE packets have been received and combined successfully, returns 1.



#### Decode the Buffer

The next step is to decode the produced buffer that has been created from the combination of the BLE packets. The following function does exactly that; that is, decodes and access the data as well as clears memory that has been allocated during this process.

```c
void DecodeBlePackets(){
    Init_Transaction_41_Structures();

    Decoder( TR41_DataContainer,
             (uint32_t)TR41_ContainerLength,
             Transaction_41_SchemaDescription );

    Wrap_Transaction_41_Structures();
    Clear_Transaction_41_Structures();
}
```

Note that the functions:

- `Init_Transaction_41_Structures()`
  - initializes various variables that will be used to get the decoded values.
- `Decoder(...)`
  - this is the heart of the CGTP library and decodes any properly created byte stream.
- `Wrap_Transaction_41_Structures()`
  - assigns the decoded data to the corresponding variables.
- `Clear_Transaction_41_Structures()`
  - clears any memory that has been allocated during the decoding process.

as well as the variables:

- `TR41_DataContainer`
- `TR41_ContainerLength`
- `Transaction_41_SchemaDescription`

are internal entities of the CGTP library.

`TR41_DecodeBlePackets() ` is called after getting a proper output (i.e. the value 1) from `AddPacketToBufferAndCheck(packet)`. 



#### Integration

At this point we will put the aforementioned functions together. Let's first suppose that there is a loop where BLE packets are received continuously after we have asked from TWD to return the run info data that are saved in the internal memory. Inside this loop, the ` AddPacketToBufferAndCheck(packet)` manages  the received packets and returns 1 only when all the needed consecutive BLE packets combined together successfully. Afterwards, the `TR41_DecodeBlePackets()`  is used to decode the created buffer. The next step, after having decoded the buffer, is to get the TWD output. This can be achieved by using the   `GetTrans3Data(DataSchema)` function, which returns a pointer (`char*`) of the a json-based string. This string <u>must be parsed with any json parser to access the TWD output</u>. Finally, we have to clear the memory that has been allocated, by using the `Clear_Transaction_3_Data()` function, in order to repeat the process again for the next three BLE packets.

```c
void Example_GetRunInfoData(){
	int i;
    //  _____  GET THE BLE PACKETS AND ADD THEIR CONTENTS TO THE BUFFER 

    int error_flag = 0;
    for (i=0; i<1000; i++){
        // simulation of the BLE packets receival process
        uint8_t* packet = SimulateBlePackets(i);

        if (packet){
            // add the packet to the buffer and check if all 
            // the packets have been received successfully.
            if ( AddPacketToBufferAndCheck(packet) == 1){
                error_flag = 0;
                break;
            }else{
                error_flag = 1;
            }
        }
    }
    // THE BLE RECEIVAL LOOP ENDS HERE  
    
    if (error_flag){
        printf("Error in receiving the ble packets.\n");
    }
    
    // DECODE THE BUFFER - if there is no error in the receival process    
    DecodeBlePackets();
    // BUILD THE JSON STRING
    char *jsonstr = DecodeTrans41Data2Runinfo();
    // CLEAR THE DATA RELATED MEMEORY
    Clear_transaction_41_Data();
    // DO SOMETHING WITH THE JSON STRING
    printf("json: %s", jsonstr);
}
```



#### Resulted Output

The above example would return a json-based string like the following one:

```json
json: {
    "NumberOfRuns": 4,
    "Run0": {
        "Slot": 0,
        "RunID":        30,
        "RunDataLength":        5,
        "ActiveDuration":       0.40300017595291138,
        "ActiveDuration_unit":  "minutes",
        "ActiveDistance":       53.468425750732422,
        "ActiveDistance_unit":  "m",
        "Calories":     5.0927219390869141,
        "Calories_unit":        "KCal",
        "AverageSpeed": 7.9605560302734375,
        "AverageSpeed_unit":    "Km/h",
        "Status":       2
    },
    "Run1": {
        "Slot": 1,
        "RunID":        38,
        "RunDataLength":        3,
        "ActiveDuration":       0.24933329224586487,
        "ActiveDuration_unit":  "minutes",
        "ActiveDistance":       32.967819213867188,
        "ActiveDistance_unit":  "m",
        "Calories":     2.9969091415405273,
        "Calories_unit":        "KCal",
        "AverageSpeed": 7.9334335327148437,
        "AverageSpeed_unit":    "Km/h",
        "Status":       2
    },
    "Run2": {
        "Slot": 2,
        "RunID":        39,
        "RunDataLength":        1,
        "ActiveDuration":       0.104166679084301,
        "ActiveDuration_unit":  "minutes",
        "ActiveDistance":       10.48656177520752,
        "ActiveDistance_unit":  "m",
        "Calories":     0.77393066883087158,
        "Calories_unit":        "KCal",
        "AverageSpeed": 6.0402588844299316,
        "AverageSpeed_unit":    "Km/h",
        "Status":       2
    },
    "Run3": {
        "Slot": 3,
        "RunID":        41,
        "RunDataLength":        10,
        "ActiveDuration":       1.0235002040863037,
        "ActiveDuration_unit":  "minutes",
        "ActiveDistance":       136.53433227539062,
        "ActiveDistance_unit":  "m",
        "Calories":     9.7040119171142578,
        "Calories_unit":        "KCal",
        "AverageSpeed": 8.0039653778076172,
        "AverageSpeed_unit":    "Km/h",
        "Status":       2
    }
}
```
